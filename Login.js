import * as React from 'react';
import { TextInput, TouchableOpacity, Text, Image, View, ScrollView, Alert, TouchableHighlight, Button, KeyboardAvoidingView} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from "./style/Login_style"
class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      Email: "",
      Password: '',
      isLoading: true,
      isDone: false,
      Apireturn: null,
    }
  }
  // variable to hold the references of the textfields
  inputs = {};
  // function to focus the field
  focusTheField = (id) => {
    this.inputs[id].focus();
  }

  static navigationOptions = {
    header: null 
  }

  //Calling Login api route
  LoginApi = async (user)=>{
    fetch(
      "http://jules.info-rapide.fr/Login",
        {
          method: "POST",
          headers: {
              "Accept": "application/json",
              "Content-Type": "application/json"
          },
          body: user
        }
      )
      .then((response) =>   response.json())
      .then((response2) => {
        this.setState({
          isLoading: false, 
          Apireturn: JSON.stringify(response2) 
        })
      })
      .catch(function(error) {
      })
  }

  //Function called after pressing a button
  handleSubmit = (e) => {
    e.preventDefault();
    //checking if every input is filled
    if (this.state.Email !== "" && this.state.Password !== "") {
      var user = {
          Email: this.state.Email,
          Password: this.state.Password
        }
      user = JSON.stringify(user)
      this.LoginApi(user);   
    }
    else {   
      Alert.alert(
        'Connexion impossible',
        'Veuillez remplir tous les champs',
      )
    }
  }

  render() {
    const { navigate } = this.props.navigation;

    if (!this.state.isLoading) {
      if (this.state.Apireturn !== null) {
        parsedResponse = JSON.parse(this.state.Apireturn); 
              if (parsedResponse.response === 1) {
                Alert.alert(
                  'Connexion impossible',
                  'Mauvaise combinaison Email / Mot de passe ',
                )
              }
              else if (parsedResponse.response == 0) {
                  global.Email = this.state.Email;
                  global.id = parsedResponse.id
                  navigate('DialogFlow')
              }
        }
        this.state.Apireturn = null;
    }

    return (
      
      <KeyboardAvoidingView contentContainerStyle={{flex:1}} scrollEnabled={true}
      style= {{flex: 1, backgroundColor: '#ffffff'}}
      behavior="position"
      enableAutoAutomaticScrol='true'
      keyboardOpeningTime={0}>
        <View style={{alignItems: "center"}}>
          <View style={styles.image}>
            <Image source={ require('./public/logo.png')} style={ {width: wp('40%'), height: wp('40%'), resiresizeMode:'contain'}} />
          </View>
          <Text style={styles.Title}>Se Connecter</Text>
          <View style={styles.ConnectDescrView}>
            <Text style={styles.connectDescr}>Connectez-vous pour retrouver votre historique de vos travaux chez nos clients !</Text>
          </View>
        </View>
        <View style={styles.Form}>
          <ScrollView>
            <TextInput 
              style={styles.input} onChangeText={Email => this.setState({ Email })}
              underlineColorAndroid={'transparent'}
              value={this.state.Email} placeholder={'Entrez votre adresse mail'}
              placeholderTextColor={'#a7a7a7'}
              autoCapitalize="none"
              blurOnSubmit={ false }
              returnKeyType={ 'next' }
              onSubmitEditing={() => { this.focusTheField('Password'); }}
            /> 
            <TextInput
              ref={input => { this.inputs['Password'] = input }}
              label={"Password"}
              style={ styles.input }
              underlineColorAndroid={'transparent'} 
              secureTextEntry={true}
              onChangeText={Password => this.setState({ Password })} 
              value={this.state.Password}
              placeholder= {'Entrer votre mot de passe'}
              placeholderTextColor= {'#a7a7a7'}
              autoCapitalize = "none"
              onSubmitEditing={this.handleSubmit}
              />
            </ScrollView>
          <TouchableOpacity onPress = {this.handleSubmit} style={styles.touchablebutton}>
            <View style={styles.Button}>
              <Text style={{ color: '#ffffff', fontSize: hp("2.4%")}}>Se connecter</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={ styles.linksView1}>
          <Text style={ styles.linkText } onPress={ () => navigate('SignupForm') } >S'inscrire</Text>
        </View>
        <View style={ styles.linksView2}>                                 
          <Text style={ styles.linkText } onPress={ () => navigate('ForgotPwd') }>Mot de passe oublié</Text>
        </View>
        <View style={ styles.copyrightView }>
          <Text style={ styles.copyright }> ®2018 - LesJules </Text>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

export default Login;