import * as React from 'react';
import { Text, View, Image, TouchableOpacity, StyleSheet,  TextInput, Alert } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from "./style/ForgotPwd_style"

class ForgotPwd extends React.Component {
  constructor(props) {
    super(props)
    this.state = { Email: '',
    isLoading: true,
    isDone: false,
    Apireturn: null,
    text: "",
    }
  }

  static navigationOptions = {
    header: null 
  }
  //Call api to update your password and show it to you
  Ask_new_password = async (user)=>{
    fetch(
      "http://jules.info-rapide.fr/ForgotPassword",
      {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        body: user
      }
    )
    .then((response) =>   response.json() )
    .then((response2) => {
      this.setState({
      isLoading: false, 
      Apireturn: JSON.stringify(response2) 
      })
    })
    .catch(function(error) {
      alert(error);
    })
  }

  //Function called after pressing a button
  handleSubmit = (e) => {
    e.preventDefault();
    if (this.state.Email !== "") {
      var user = {
        Email: this.state.Email,
      }
      user = JSON.stringify(user)
      var myvalue = this.Ask_new_password(user);   
    }   
    else {    
      Alert.alert("Veuillez entrez votre adresse Email")
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    if (!this.state.isLoading) {
      if (this.state.Apireturn !== null) {
        parsedResponse = JSON.parse(this.state.Apireturn);
        if (parsedResponse.response === 1) {
          Alert.alert(
            "Email Incorrect",
            "Cet Email n'existe pas"
            )
        }
        else {
          console.log("password is good", parsedResponse)
          this.setState({Email: parsedResponse});
          this.setState({text: "Voici votre nouveau mot de passe surtout ne le perdez pas"});
        }
      }
      this.state.Apireturn = null;
    }
    return (
      <View style={styles.container}>
        <View style={{alignItems: "center"}}>
          <View style={styles.image}>
            <Image source={ require('./public/logo.png')} style={{ height: hp('17%'), width: hp('28%'),resizeMode:'contain' }}/>
          </View>
          <Text style={styles.title}>Mot de passe oublié</Text>
          <View style={styles.titleDescrView}>
            <Text style={styles.titleDescr}>Un trou de mémoire ? c'est pas grave, entrez votre email pour recevoir votre nouveau mot de passe. Changez-le dans "mon compte".</Text>
          </View>
        </View>
        <View style={styles.Form}>
          <View style={styles.input}>
          <Text> {this.state.text}</Text>
            <TextInput onChangeText={Email => this.setState({ Email })} 
                value={this.state.Email} placeholder= {'Entrez votre adresse mail'}
                placeholderTextColor= {'#a7a7a7'}
                underlineColorAndroid= 'transparent'
                style={{color: "black", fontSize: wp("4.5%"), fontFamily: "CircularBold",}}
                autoCapitalize = "none"
            />
          </View>
        </View>
        
        <TouchableOpacity onPress = {this.handleSubmit} style={styles.touchableButton}>
          <View style={styles.Button}>
            <Text style={{ color: '#ffffff',  fontSize: hp("2.4%")}}>valider</Text>
          </View>
        </TouchableOpacity>
        <View style={ styles.linksView1}>
          <Text style={ styles.linkText } onPress={ () => navigate('SignupForm') } >S'inscrire</Text>
        </View>
        <View style={ styles.linksView2}>                                 
          <Text style={ styles.linkText } onPress={ () => navigate('Login') }>Retour</Text>
        </View>
      </View>
);
  }
}


export default ForgotPwd;