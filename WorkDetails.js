import React, { Component } from 'react';
import { Platform, TextInput, TouchableOpacity, Text, ScrollView, KeyboardAvoidingView, View, Modal, TouchableHighlight, Image, Alert } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import NavigationBar from 'react-native-navbar';
import PopupDialog, {
  SlideAnimation,
  ScaleAnimation,
  FadeAnimation,
} from 'react-native-popup-dialog';
import styles from './style/WorkDetails_style'

const slideAnimation = new SlideAnimation({ slideFrom: 'bottom' });
const scaleAnimation = new ScaleAnimation();
const fadeAnimation = new FadeAnimation({ animationDuration: 150 });

export default class WorkDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      isLoading: true,
      dataSource: null, 
      isLoading2: true,
      dataSource2: null,
      gestureName: 'none',
      dialogShow: false,
      imagesRobot : "none",
      imageRobotMessage : "none",
      timeSec : 0,
      travaux: "",
      note: "",
      state: 0,
      statevalue: 2,
      duree: 0,
      hour: "0h0m0s", 
      discussion: "",
      selHours : 0, 
      selMinutes : 0,
      selTimer: 0,
      timer: {        
        inputvalue: "Start",
        inputvalue1: "Fini",
        worktime : -1,
        lastevent: 0
      },
      reload: 0,  
    }
    this.dotimerFlag = false;
    this.finalworktime = 0
    this.scrollview = null;
    data = [];
  }
  
  static navigationOptions = {
    header: null 
  }
  
  //Call api route that return the task with the id sent
  get_intervention = async (intervention) => {
    fetch(
      "http://jules.info-rapide.fr/Get_Details_Interventions",
      {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        body: intervention
      }
    )
    .then((response) =>   response.json() )
    .then((response2) => {
    this.setState({
      isLoading: false, 
      dataSource: JSON.stringify(response2)
    })

    })
    .catch(function(error) {
    })
  }
  Update_note = async () => {
    fetch(
      "http://jules.info-rapide.fr/Update_Interventions_Note",
      {
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          id: this.props.navigation.state.params.data.id,
          state: this.state.state,
          note: this.state.note,
          travaux: this.state.travaux,          
        })
      }
    )
    .then((response1) =>   response1.json() );    
  };

  //Allow user to update the information in this task
  Update_work = async () => {
    fetch(
      "http://jules.info-rapide.fr/Update_Work",
      {
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          id: this.props.navigation.state.params.data.id,
          duree: this.state.timer.worktime,
          etattimer: this.state.timer.inputvalue,
          status: this.state.timer.lastevent,
          state: this.state.state,          
        })
      }
    )
    .then((response1) =>   response1.json() );    
  };
  Update_Timer = async () => {
    fetch(
      "http://jules.info-rapide.fr/Update_Work",
      {
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          id: this.props.navigation.state.params.data.id,
          duree: this.state.timer.worktime,
          etattimer: this.state.timer.inputvalue1,
          status: this.state.timer.lastevent,
          state: this.state.state,          
        })
      }
    )
    .then((response1) =>   response1.json() );
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  setselTimer() {
    this.state.timer.worktime = this.state.timer.worktime + this.state.selHours*3600
    this.state.timer.worktime = this.state.timer.worktime + this.state.selMinutes*60
    this.setState({state: 1});
  }
  componentDidMount() {
    this.dotimerFlag = true;
  }
  componentWillMount() {
    var intervention = {
      IDintervention: this.props.navigation.state.params.data.id
    };
    intervention = JSON.stringify(intervention) 
    this.get_intervention(intervention)
  }

  showScaleAnimationDialog = () => {
      this.setState({isLoading:true}); 
    this.scaleAnimationDialog.show();
  }
  showSlideAnimationDialog = () => {
    this.slideAnimationDialog.show();
  }
  showFadeAnimationDialog = () => {
    this.fadeAnimationDialog.show();
  }

  increaseMinutes = () => {
    minutes = this.state.selMinutes;
    minutes++;
    
    while (minutes >= 60) {
      minutes -=60;
    }
    this.setState({ 
      isLoading: true, 
      selMinutes : minutes 
    });
  }

  decreaseMinutes = () => {
    minutes = this.state.selMinutes;
    minutes--;

    while (minutes < 0) {
      minutes +=60;
    }
    this.setState({ 
      isLoading: true, 
      selMinutes : minutes 
    });
  }

  increaseHours = () => {
    hours = this.state.selHours;
    hours++;

    while (hours >= 24) {
      hours -=24;
    }
    this.setState({ 
      isLoading: true, 
      selHours : hours
    });
  }

  decreaseHours = () => {
    hours = this.state.selHours;
    hours--;
    while (hours < 0) {
      hours +=24;
    }
    this.setState({ 
      isLoading: true, 
      selHours : hours 
    });
  }

  forceRemount = () => {
    this.setState({
      isLoading : true,
    })
    this.state.state = 1
    this.Update_Timer();
    this.componentWillMount()
  }


  tohms = (data) => {
    worki = data;
    h = (worki / 3600) | 0 ;
    worki = worki % 3600;
    m = (worki / 60 ) | 0;
    s = worki % 60 ;
    if (h < 10) {
      h = "0" + h
    }
    if (m < 10) {
      m = "0" + m
    }
    if (s < 10) {
      s = "0" + s
    }
    hms = h + "h" + m +"m" + s+"s";
    return hms;
  }
  TIMER = () => {
    const { navigate } = this.props.navigation;
    return(
      <View>
      <View style={ styles.time }>
      <View style={ styles.timePart }>
        <TouchableOpacity onPress= { this.increaseHours}>
          <Image source={ require('./public/up.png')}/>
        </TouchableOpacity>
        <Text style={ styles.timePartValue }>{ this.state.selHours < 10 ? "0" : ""}{ this.state.selHours }</Text>
        <TouchableOpacity onPress= { this.decreaseHours }>
          <Image source={ require('./public/down.png')}/>
        </TouchableOpacity>
      </View>
      <View>
        <Text style= { styles.timePartUnit }>H</Text>
      </View>
      <View style= { styles.timePart }>
        <TouchableOpacity onPress= { this.increaseMinutes}>
          <Image source={ require('./public/up.png')}/>
        </TouchableOpacity>
        <Text style={ styles.timePartValue }>{ this.state.selMinutes < 10 ? "0" : ""}{ this.state.selMinutes }</Text>
        <TouchableOpacity onPress= { this.decreaseMinutes}>
          <Image source={ require('./public/down.png')}/>
        </TouchableOpacity>
      </View>
      <View>
        <Text style={ styles.timePartUnit }>M</Text>
      </View>

    </View>
          <View style= {{ flexDirection: 'row', marginTop: hp('3%'), justifyContent: 'center' }}>
      
          <TouchableOpacity onPress = {() => { 
            this.setModalVisible(!this.state.modalVisible);
          }}>
            <View style= { styles.buttonclose }>
                <Text style={ styles.buttonTextclose }>Annuler</Text>
              </View>
          </TouchableOpacity>
          <TouchableOpacity onPress = {() => {
              this.setselTimer();
              this.setModalVisible(false);
              this.state.timer.inputvalue = "Fini";
              this.dotimerFlag = false;
              this.forceRemount();
          }}  
            style={{ borderRadius: hp('9%')}}
            title={ 'OK' }>
            <View style= { styles.buttonvalider }>
              <Text style={ styles.buttonTextvalider }>Valider</Text>
            </View>
            
          </TouchableOpacity>          
        </View>
        </View>
    );
  }

  dottimer = () => {
    ///!< do timer here
    setTimeout(() => {   
      if ( this.dotimerFlag ) {
        ///!< on timer mode  
        ///!< get time from begin ( work directle on worktime en second )
        hms = this.tohms(this.state.timer.worktime);
        this.setState({hour: hms})  

        // ici  
        //this.reftime.value = 
        
        this.setState( {isLoading : true , timer : { inputvalue : this.state.timer.inputvalue ,  worktime : this.state.timer.worktime+1 } } );                    
        this.dottimer();
      }
    }, 1000); 
  };

  InProgress() {
    const { navigate } = this.props.navigation;
    if (this.state.state == 0) {
      return(
        <View style={{alignItems: "center"}}>
          <View style={{ alignItems: "center", width: hp("10%"), position: "absolute", top: hp("-11%"), right: hp("0%")}}>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.modalVisible}
            >
            <View style={{alignItems: "center"}}>
              <View elevation={5}  style={styles.Modale}>
                <View>
                  <Text style={styles.titlemodal}>Durée de l'intervention</Text>
                    {this.TIMER()}
                  </View>
                </View>
            </View>
            </Modal>
            <TouchableOpacity
              style={{top:hp("-3%")}}
              onPress={() => {
                this.setModalVisible(true);
              }}>
              <Image source= { require('./public/clock.png') } />
            </TouchableOpacity>
            </View>
          
        <View style= { styles.buttons }>
          <TouchableOpacity 
            onPress={() => {
            if (this.state.timer.inputvalue == "Start") {
              // Timer();
              this.dotimerFlag = true;
              //this.state.timer.Start = "Pause";
              if ( this.state.timer.worktime < 0 ) {
                this.setState( {isLoading : true, timer : { inputvalue : "Pause" , worktime :  0 , lastevent : new Date() } }); 
                setTimeout(() => { this.Update_work();  this.Update_note();   }, 500); 
              }
              else {
                this.setState( {isLoading : true, timer : { inputvalue : "Pause" , worktime : this.state.timer.worktime , lastevent : new Date() }});
                setTimeout(() => { this.Update_work();  this.Update_note();   }, 500); 
              }
              this.dottimer();
            } 
            else {
              this.dotimerFlag = false;
              //this.state.timer.clicked = 1
              this.setState( {isLoading : true, timer : { inputvalue : "Start", worktime : this.state.timer.worktime , lastevent : new Date() } });
              setTimeout(() => { this.Update_work();  this.Update_note();   }, 500); 
              //this.state.timer.Start = "Start";
            }  
          }}>
        
          <View style={{ borderWidth: 3, width: hp('14%'), height: hp('14%'), borderRadius: hp('14%'), borderColor: '#4a90e2', alignItems: 'center', justifyContent: 'center', marginRight: wp('2%') }}>
            <Text style={{ color: '#4a90e2', fontSize: hp('4%')}}>{this.state.timer.inputvalue}</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity 
            // style={styles.touchablebutton}
            onPress={() => {
            this.dotimerFlag = false;
            this.setState({isLoading : true, state : 1 ,  timer:{inputvalue1:"Fini",  worktime : this.state.timer.worktime, lastevent : new Date() }})
            setTimeout(() => { this.Update_work();  this.Update_note();  navigate("Listing"); }, 500);
          }}>
            <View style={{ borderWidth: 3, width: hp('14%'), height: hp('14%'), borderRadius: hp('14%'), borderColor: '#417505', alignItems: 'center', justifyContent: 'center', marginLeft: wp('10%') }}>
              <Text style={{ color: '#417505', fontSize: hp('4%')}}>Fini</Text>
            </View>  
          </TouchableOpacity>   
        </View>
        </View>
      );
    }
  }

  render() {

    if (this.dotimerFlag) {

      //this.dottimer();
    }
    const { navigate } = this.props.navigation;
    const myRightTitle = <Text style={styles.titleNav}>Finir</Text>;
    if (!this.state.isLoading) {  
      if (this.state.dataSource !== null) {
        parsedResponse = JSON.parse(this.state.dataSource);
        console.log("Test ?");
        
        if (parsedResponse.response === 1) {
          console.log("pas de tache");  
        }
        else {
          console.log("state of timer : " + parsedResponse[0].EtatTimer );
            this.state.travaux = parsedResponse[0].travaux;
            this.state.timer.worktime = parseInt(parsedResponse[0].duree);
            this.state.note = parsedResponse[0].note;
            this.state.state = parseInt(parsedResponse[0].state);            
            this.state.discussion = JSON.parse(parsedResponse[0].discussion);             
            this.state.timer.inputvalue = parsedResponse[0].EtatTimer;        
            this.state.timer.inputvalue1 = parsedResponse[0].EtatTimer;     
            this.state.dataSource = null;            
            this.setState({hour: this.tohms(this.state.timer.worktime)});
            if ( this.state.timer.inputvalue == "Pause" ) {
              this.dotimerFlag =true; 
              this.dottimer();
            }
        }
      }
    }
    //---<Navbar configuration>---//
    const myLeftTitle = <Text style={styles.titleNav}>Retour</Text>;
    const leftButtonConfig = {
      title: myLeftTitle,
      handler: () => { 
        this.Update_note();
        this.Update_work();
        console.log("RETOUR")
        navigate("Listing");
      } 
    };
    //---</Navbar configuration>---//

    return(
      <KeyboardAvoidingView  contentContainerStyle={{flex:1}} scrollEnabled={true}
        behavior='padding'
        style= {{flex: 1}}
        enableAutoAutomaticScrol='true'
        keyboardOpeningTime={0}
      >
        <NavigationBar
          style={styles.Navbar}
          leftButton={leftButtonConfig}
        />
        <ScrollView  
          style={{flex: 1, backgroundColor: "white"}} 
          ref={ref => this.scrollView = ref} 
        >
          <View style={ styles.header}>
            <TextInput 
              style= { styles.title }
              onChangeText={travaux => this.setState({ travaux })} 
              value={this.state.travaux}>
            </TextInput>
            </View>
            <View style= {styles.body }>
              <View style={styles.mytitle}>
                <Text style={{ fontSize : hp('2.5%'), fontWeight : 'bold'}}>Adresse du client</Text>
              </View>
              <View style={ styles.adressView}>
                <TextInput
                  style= { styles.note }
                  multiline = {true}
                  numberOfLines = {4}
                  onChangeText={note => this.setState({ note })}
                  placeholder = "Saisissez ici les coordonnées du client" 
                  value = {this.state.note }     
                />
                </View>
                <View style={{alignItems: "center", marginTop: hp("3%"), justifyContent: 'center', textAlignVertical: 'center'}}>
                  <TouchableOpacity 
                  style={styles.save}
                  onPress = {() => { 
                    this.Update_note();
                    this.Update_work()
                  }}>
                    <Text style={{color: "white", justifyContent: 'center', textAlignVertical: 'center'}}>Sauvegarder</Text>
                  </TouchableOpacity>    
                </View>         
              </View>   
              <View>    
                <View style={styles.mytitle1}>
                  <Text style= {{ fontSize: hp('2.5%'), fontWeight: 'bold'}}>Travaux</Text>         
                </View>   
                <View style={ { alignItems: 'center' }}>
                  <View style={ styles.timerView }>
                    <Text style={ styles.timerText }> {this.state.hour}</Text>
                    {/* {this.state.hour} */}
                  </View>
                </View>
                {this.InProgress()}
                <View style={{ marginTop: hp('5%'), alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontSize: hp('2.5%'), fontWeight: 'bold'}}>Historique de conversation</Text>
                </View>                     
                <View style={ styles.discussion }>
                  { this.state.discussion ? ( 
                    this.state.discussion.map(function(d,i) { 
                        return (
                          <View>
                            {d.question !== null && <View style={styles.dialogQ}><Text style={{color:"#000000"}}>Julie : </Text><Text style={{ fontStyle: 'italic', color: '#585858'}}>{d.question}</Text></View> }
                        <View style={styles.dialogA}><Text style={{color:"#000000"}}>Vous : </Text><Text style={{ color: '#000000'}}>{d.answer}</Text></View>                            
                          </View>
                        )
                      
                      
                    })
                  ):
                <Text> </Text>  
              }          
            </View> 
          </View> 
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

