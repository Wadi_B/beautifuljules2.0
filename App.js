import { createStackNavigator } from 'react-navigation';
import ForgotPwd from './ForgotPwd';
import Start from './Start';
import Start2 from './Start2';
import Login from './Login';
import UserPage from './UserPage';
import UserModify from './UserPageModify';
import DialogFlow from './DialogFlow';
import SignupForm from './SignupForm';
import Listing from './Listing';
import WorkDetails from './WorkDetails';

//desactivation de l'affichage des warinings
console.disableYellowBox = true;

//Loading de la police
Expo.Font.loadAsync({
  "Circular": require('./assets/fonts/CircularStd-Book.otf'),
  "CircularBold": require('./assets/fonts/CircularStd-Bold.otf'),
});
const customTextProps = { 
  style: { 
    fontFamily: "Circular",  
  }
}
//Declaration de la navigation
const App = createStackNavigator({
  Start: { screen: Start },
  Start2: { screen: Start2 },
  Login: { screen: Login },
  SignupForm: { screen: SignupForm },
  ForgotPwd: { screen: ForgotPwd }, 
  DialogFlow: {screen: DialogFlow},
  UserPage: {screen: UserPage},
  UserModify: {screen: UserModify},
  Listing: {screen : Listing},
  WorkDetails: { screen : WorkDetails },
});

export default App;