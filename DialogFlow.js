import React, { Component } from 'react';
import Modal from 'react-native-modal';
import { TouchableOpacity, Keyboard, Picker, Platform, Alert, StyleSheet, Text, TextInput, Button, ScrollView, KeyboardAvoidingView, View, Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import NavigationBar from 'react-native-navbar';
import styles from "./style/DialogFlow_style";
import { Buffer } from 'buffer';
import PopupDialog, {
  DialogTitle,
  DialogButton,
  SlideAnimation,
  ScaleAnimation,
  FadeAnimation,
} from 'react-native-popup-dialog';
import {Header, Left} from "native-base"

const slideAnimation = new SlideAnimation({ slideFrom: 'bottom' });
const scaleAnimation = new ScaleAnimation();
const fadeAnimation = new FadeAnimation({ animationDuration: 150 });

export default class DialogFlow extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isLoading2 : true,
      Apireturn: null,
      Apireturn2 : null,
      gestureName: 'none', 
      dialogShow: false,
      imagesRobot : "none",
      imageRobotMessage : "none",
      pickerAns: null,
      msgInput : "",
      modalMsgInput: "",
      selHours : 0,
      selMinutes: 0,
      end : false,
      keyboardType : 'default',
      idIntervention : -1,
      questionChoices : false,
      modalVisible: false
    }
    this.scrollview = null;
    this.onlyString = true;
    this.inputAns = null;
    this.modalInputAns = null;
    data = [];
    otherAnswers = [];
    this.pickerOptions = [];
    this.workDoneQuestion = false;
    this.workDoneAnswer = -1;
    this.workTypeQuestion = false;
    this.workTypeAnswer = null;
    this.dialog = [];
    this.currentQuestion = null;
    this.currentChoices = [];
    this.beginDate = null;
    this.endDate = null;
    this.idIntervention = -1;
  }

  /* insert data to server */
  insert_to_server = async ( elt )=>{
    var dis = "";
    this.dialog.forEach(element => {
      dis+=element.question+"$"+element.response ;
    });
    var test = fetch(
      "http://jules.info-rapide.fr/Insert_Interventions",
      {
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(elt)
        }
      )
      .then((response1) =>  response1.json() )
      .then((response3) => {
        console.log("[insert_to_server] response3="+JSON.stringify(response3));
          this.setState({
            isLoading: true,
            isLoading2: false, 
            Apireturn2: response3,
            idIntervention : response3[0].id
          });
        this.idIntervention = response3[0].id;   
      })
    .catch(function(error) {
    });
    console.log("fetch", test)
  }

  // update discussion data
  update_to_server = async ( elt ) => {
    fetch(
      "http://jules.info-rapide.fr/Update_Only_Discussion",
      {
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(elt)
      })
      .then((response1) =>  response1.json() )
      .then((response3) => {
          this.setState({
            isLoading: true,
            isLoading2: false, 
            Apireturn2: response3
        });
      })
      .catch(function(error) {
      });
  }


  test_question_answer() {
    console.log("test_question_answers");
    console.log(JSON.stringify(this.currentChoices));
    fetch(
      'http://jules.info-rapide.fr/Other_Answers',
      {
        method: "POST",
        headers: {
          "Accept": "application/octet-stream",
          "Content-Type": "application/octet-stream"
        },
        body: Buffer.from(JSON.stringify(otherAnswers),'utf8')
      })
      .then((response1) => { response1.json()})
      .then((response2) => {
        console.log("[test_question_answer] "+ response2);

      })
      .catch((error) => {
        console.error("test_question_answer "+error);
      }); 
  }

  // disable toolbar
  static navigationOptions = {
    header: null 
  }
  
  //----<Timer functions>----//
  increaseMinutes = () => {
    minutes = this.state.selMinutes;
    minutes++;
    
    while (minutes >= 60) {
      minutes -=60;
    }
    this.setState({ 
      isLoading: true, 
      selMinutes : minutes 
    });
  }

  decreaseMinutes = () => {
    minutes = this.state.selMinutes;
    minutes--;

    while (minutes < 0) {
      minutes +=60;
    }
    this.setState({ 
      isLoading: true, 
      selMinutes : minutes 
    });
  }

  increaseHours = () => {
    hours = this.state.selHours;
    hours++;

    while (hours >= 24) {
      hours -=24;
    }
    this.setState({ 
      isLoading: true, 
      selHours : hours
    });
  }

  decreaseHours = () => {
    hours = this.state.selHours;
    hours--;
    while (hours < 0) {
      hours +=24;
    }
    this.setState({ 
      isLoading: true, 
      selHours : hours 
    });
  }
  //----</Timer functions>----//
  
  showScaleAnimationDialog = () => {
    this.setState({isLoading:true}); 
    this.scaleAnimationDialog.show();
  }
  showSlideAnimationDialog = () => {
    this.slideAnimationDialog.show();
  } 

  showFadeAnimationDialog = () => {
    this.fadeAnimationDialog.show();
  }
  componentDidMount() {
    this.Call_DialogFlow_Api("Bonjour");
    this.onlyString = true;
    this.beginDate = new Date() ;
  }

  //reload the page
  reset() {
    this.props.navigation.navigate('DialogFlow');
  }

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n) ;
  }

  save(data) {
  }

  setModalVisible(visible=true) {
    this.setState({modalVisible : visible});
  }


  Call_DialogFlow_Api(msg, other="") {

    ///!< What is this
    var dis = "";
    this.dialog.forEach(element => {
      dis+=element.question+"$"+element.response ;
    });
    /// we have a id of server hihi
    /// juste add this line to dialog
    if (this.state.Apireturn2) {
      this.update_to_server({ 
        id : this.idIntervention,
        DateFin : new Date(),
        discussion : this.dialog
      });
    }
    data.forEach((elt) => { elt.unset = false});

    ///!< init text input for user
    if (this.inputAns != null) {
    if (Platform.OS === 'ios')
      this.inputAns.setNativeProps({ text: ' ' });
    else
      this.inputAns.clear();

    setTimeout(() => { this.inputAns.setNativeProps({ text: '' }) }, 5);

    }

    this.dialog.push({ question : this.currentQuestion, answer : other.trim() != "" ? other : msg});
    console.log(JSON.stringify(this.dialog));

    if (this.workDoneQuestion) {  
      if (msg.toLowerCase().trim() === 'oui') {
        this.workDoneAnswer = 1;
      }
      if (msg.toLowerCase().trim().startsWith('non')) {
        this.workDoneAnswer = 0;
      }
      this.workDoneQuestion = false;
    }

    if (this.workTypeQuestion) { 
      this.workTypeAnswer = msg; 
      this.workTypeQuestion = false;
      if (!this.workDoneAnswer) { 
        this.insert_to_server({ 
          travaux : this.workTypeAnswer, 
          state : 0, 
          DateDebut : this.beginDate, 
          idUser : global.id, 
          DateFin : "", 
          discussion : this.dialog, 
          duree:0, 
          dateInsertion: ""
        });
      }
    }

    if (this.state.end) {
      this.setState({ 
        end:false, 
        selHours : 0, 
        selMinutes : 0 , 
        idIntervention : this.idIntervention 
      });
    }

    if (this.onlyString && this.isNumeric(msg)) { 
      Alert.alert("Les nombres ne sont pas acceptés.");
      return;
    }
    this.onlyString = false;
    data.push({ "vous" : other.trim() != "" ? other : msg });
    res = fetch('https://api.dialogflow.com/v1/query?v=20150910', {
      method: 'POST',
      headers: {
        'Authorization' : 'Bearer 9aa3c65e64684f9788949cf7f637b11d',
        'Content-Type': 'application/json',
      },
      body :  JSON.stringify  ({
        "contexts" : ["awaiting_name"],
        "lang" : "en",
        "query" : msg.replace("é","e").replace('è','e').replace('à','a'),
        "sessionId": "chatbot-red",
      })
    })
    .then((response) => response.json())
    .then((response2) => {
      this.setState({
        isLoading: false,
        Apireturn: response2,
        idIntervention : this.idIntervention
    });
    if (this.inputAns !=null) {
      this.inputAns.clear(); 
      this.inputAns.setNativeProps({ text: '' });
      Keyboard.dismiss();
    }
    }).catch((error) => {
      Alert.alert(error);
    });
    return res;
  }
  
  doClear() {
    let textInput = this.refs["textInput"];
    textInput.clear();
  }



  render(){
    const { navigate } = this.props.navigation;
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };
    //--<NavBar Config>--//
    const myLeftTitle = <Text style={styles.titleNav}>Mon compte</Text>;
    const myRightTitle = <Text style={styles.titleNav}>Suivi</Text>;
    const leftButtonConfig = {
      title: myLeftTitle,
      handler: () => navigate("UserPage"),
    };

    const rightButtonConfig = {
      title: myRightTitle,
      handler: () => navigate("Listing"),
    };
        //--</NavBar Config>--//

    if (!this.state.isLoading2) {
      let mybody = {
        travaux : this.workTypeAnswer,
        duree : (this.state.selHours*3600+this.state.selMinutes*60),
        state : (this.state.end ? 1 : 0),
        discussion : this.dialog
      }
      parsedResponse = this.state.Apireturn2;
    }
    
    if (!this.state.isLoading) { 
      if (this.state.Apireturn.result.fulfillment.speech.indexOf('^') >= 0) {
        console.log("[render] end of chat")
        this.setState({end:true,isLoading:true});
          if (! this.workDoneAnswer) {
            this.endDate = new Date();
            this.update_to_server({ 
              id : this.idIntervention, 
              travaux : this.workTypeAnswer, 
              state : 0, 
              DateDebut : this.beginDate, 
              idUser : global.id, 
              duree : 0, 
              discussion : this.dialog 
            });
          }
          else {
            this.insert_to_server({ 
              travaux : this.workTypeAnswer, 
              state : 1, 
              DateDebut : this.beginDate, 
              idUser: global.id, 
              duree : this.state.selHours*3600+this.state.selMinutes*60, 
              discussion : this.dialog 
            });
          }
        this.state.Apireturn.result.fulfillment.speech=this.state.Apireturn.result.fulfillment.speech.replace("^","");
      }
      speechOriginal = this.state.Apireturn.result.fulfillment.speech;
      speech = speechOriginal.split("@");

      // if question has choices, the bottom input must not be displayed
      if (speechOriginal.indexOf("@") >= 0 || speechOriginal.indexOf("£") >=0) {
        this.setState({questionChoices : true, isLoading: true});
      }

      else this.setState({questionChoices : false, isLoading: true});

      // message to separate in several parts
      if (speech[0].indexOf('$') >= 0) {
        parts = speech[0].split('$');
        for (i=0; i< parts.length-1; i++) {
          data.push({"julieWithoutPict" : parts[i], "unset" : true});
        }
        this.currentQuestion = parts[parts.length-1];
        data.push({"julie" : this.currentQuestion, "unset" : true});
      }

      // question if work are done
      else if( speech[0].indexOf('~') >= 0) {
        this.workDoneQuestion = true; 
        this.currentQuestion = speech[0].replace('~','');
        data.push({"julie" : this.currentQuestion, "unset" : true});
      }

      // question time
      else if (speech[0].indexOf('£') >= 0) {
        this.currentQuestion = speech[0].replace('£','');
        data.push({"julie" : this.currentQuestion, "unset": true});
        data.push({"time" : "time", "unset" : true});
      }

      // question work type
      else if (speech[0].indexOf('%') >= 0) {
        this.workTypeQuestion = true;
        this.currentQuestion = speech[0].replace('%','').replace('#','');
        data.push({"julie" : this.currentQuestion, "unset" : true});
      }

      // question with numberChoices
      else if (speech[0].indexOf('€€') >=0) {
        this.currentQuestion = speech[0].replace('€€','');
        data.push({"julie" : this.currentQuestion, "unset" : true});
        choices = ["1","2","3","4","5"];
        data.push({"numberChoices" : choices});
        data.push({"numberChoices" : ["6","7","8","9","10"]});
      }

      // picker symbol
      else {
        this.currentQuestion = speech[0].replace('#','');
        data.push({"julie" : this.currentQuestion,"unset" :true});
      }

      if (speech.length > 1) {
        choices = speech[1].split("*");
        choicesTxt = [];
        this.currentChoices = [];
        for (i=0; i< choices.length; i++) {
          // choice with images
          if (choices[i].indexOf("|") !== -1) {
            images = choices[i].split("|");
            for (j=0; j< images.length; j=j+2) {
              data.push({"choice" : images[j],"image" : images[j+1],"unset" :true});
              this.currentChoices.push(images[j+1]);
            }

          }
          else {
            data.push({"choice" : choices[i],"unset" :true});
            this.currentChoices.push(choices[i]);
          }
        }
        if (choicesTxt.length > 0) data.push({pickerChoices : choicesTxt});       
      }
    }

    return(

      <KeyboardAvoidingView  contentContainerStyle={{flex:1}} scrollEnabled={true}
        style= {{flex: 1, backgroundColor: '#ffffff'}}
        behavior="position"
        enableAutoAutomaticScrol='true'
        keyboardOpeningTime={0}
      >
        <NavigationBar
          style={styles.Navbar}
          leftButton={leftButtonConfig}
          rightButton={rightButtonConfig}
        />
        <View style={{ backgroundColor: "blue", alignItems: "center", width: hp("20%")}}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
         >
          <View style={{alignItems: "center"}}>
            <View elevation={5}  style={styles.Modale}>
              <View>
                <Text style={styles.titlemodal}>Autre choix</Text>
              </View>
              <View style={{ }}>
                <TextInput
                  style={styles.modaleinput}
                  ref={ (ref) => {this.modalInputAns = ref; } }
                  value = { () => { this.state.modalMsgInput }}
                  onChangeText = { (v) => { this.state.modalMsgInput =v }}            
                  placeholder="Saisissez votre choix">
                </TextInput>
              </View>
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                <TouchableOpacity 
                  onPress = {() => { if (this.state.modalMsgInput.trim().length > 0) { otherAnswers = {question : this.currentQuestion, choices: JSON.stringify(this.currentChoices), answer : this.state.modalMsgInput}; this.test_question_answer(); this.Call_DialogFlow_Api("Autre", this.state.modalMsgInput); this.modalInputAns.clear(); }  this.setModalVisible(!this.state.modalVisible)}}
                  style={styles.touchablebuttonmodal}>
                  <View style={styles.Buttonmodal}><Text style={{ color: '#ffffff', fontSize: hp("2.4%")}}>Valider</Text></View>
                </TouchableOpacity>
                <TouchableOpacity 
                  onPress = {() => {  this.setModalVisible(!this.state.modalVisible)}}
                  style={styles.touchablebuttonmodal}>
                  <View style={styles.Buttonmodal}><Text style={{ color: '#ffffff', fontSize: hp("2.4%")}}>Annuler</Text></View>
                </TouchableOpacity>
              </View>  
            </View>
          </View>
        </Modal>
      </View>

        <PopupDialog
          ref={(popupDialog) => {
            this.scaleAnimationDialog = popupDialog;
          }}
          dialogAnimation={scaleAnimation}
          dialogTitle={<DialogTitle title={this.state.imagesRobotMessage} />}
          actions={[
            <Button
                icon={{ 
                  name: 'arrow-right',
                  size: 15,
                  color: 'white' 
                }}
                onPress={() => {
                  this.scaleAnimationDialog.dismiss();
                }}
                title='Fermer'
                key ='key-1'
              />,
          ]}
        >
          <View id='Modal' style={styles.dialogContentView}>
            <Image  source={{ uri:'http://jules.info-rapide.fr/image?code='+this.state.imagesRobot+''}} style={styles.imageChoiceFull}/>
          </View>
        </PopupDialog>
        <ScrollView  
          style={{flex: 1}} 
          ref={ref => this.scrollView = ref} 
          onContentSizeChange={(contentWidth, contentHeight) => {        
            this.scrollView.scrollToEnd({animated: true});
          }}
        >

          <View style={ styles.header}>
            <Image source={ require('./public/logo.png')} style={{ width: hp('15%'), height: hp('14%'), top: hp("2%") }}/>
            <Image source={ require('./public/julie.png')} style={{ width: hp('15%'), height: hp('15%'), marginTop: hp('6%') }}/>
            <Text style= {styles.title }>Julie</Text>
            <Text style= {styles.descr }>Assistante travaux</Text>
          </View>
          <View style= {styles.body }>
            { data.map((prop, key) => {
              if (key === 0)   return <View key={"v00_"+key}>
          </View>    
          // our answers
          if (prop.vous) { 
            return (
              <View key={"v00_"+key} style= {styles.vous }>
                <View style={styles.vousText}>
                    <Text>{ prop.vous }</Text>
                </View>
                <Image source={ require('./public/vous.png')} style={{ width: hp('4%'), height: hp('4%'), marginLeft: wp('1.5%') }}/>
              </View>
            )}
          // julie's messages in several parts
          else if (prop.julieWithoutPict) {
            return (
            <View key={"v00_"+key} style= {styles.julie} >
              <View style={{ borderRadius: hp('4%')}}>
                <Image source={ require('./public/empty.png')} style={{ width: hp('4%'), height: hp('4%'), marginRight: wp('1.5%') }}/>
              </View>
              <View style={styles.julieText }>
                <Text>{ prop.julieWithoutPict }</Text>
              </View>
            </View>
            )}
          // julie's questions
          else if (prop.julie) { 
            return ( 
            <View key={"v00_"+key} style= {styles.julie} >
              <View style={{ borderRadius: hp('4%')}}>
                <Image source={ require('./public/julie.png')} style={{ width: hp('4%'), height: hp('4%'), marginRight: wp('1.5%') }}/>
              </View>
              <View style={styles.julieText }>
                <Text>{ prop.julie }</Text>
              </View>
            </View>
            )}
          // time select before answer (enabled)
          else if (prop.time && prop.unset == true ) {
            return  (
            <View key={"v00_"+key} style={ styles.time }>
              <View style={ styles.timePart }>
                <TouchableOpacity onPress= { this.increaseHours}>
                  <Image source={ require('./public/up.png')}/>
                </TouchableOpacity>
                <Text style={ styles.timePartValue }>{ this.state.selHours < 10 ? "0" : ""}{ this.state.selHours }</Text>
                <TouchableOpacity onPress= { this.decreaseHours }>
                  <Image source={ require('./public/down.png')}/>
                </TouchableOpacity>
              </View>
              <View>
                <Text style= { styles.timePartUnit }>H</Text>
              </View>
              <View style= { styles.timePart }>
                <TouchableOpacity onPress= { this.increaseMinutes}>
                  <Image source={ require('./public/up.png')}/>
                </TouchableOpacity>
                <Text style={ styles.timePartValue }>{ this.state.selMinutes < 10 ? "0" : ""}{ this.state.selMinutes }</Text>
                <TouchableOpacity onPress= { this.decreaseMinutes}>
                  <Image source={ require('./public/down.png')}/>
                </TouchableOpacity>
              </View>
              <View>
                <Text style={ styles.timePartUnit }>M</Text>
              </View>
              <View>
                <TouchableOpacity 
                  style={{ borderRadius: hp('9%')}}
                  onPress={ () => { this.Call_DialogFlow_Api(this.state.selHours+"h"+' '+this.state.selMinutes+"min"); }  }
                  title={ 'OK' }>
                  <View style= { styles.button }>
                    <Text style={ styles.buttonText }>✓</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            )}

              // time select after select (disabled)
              else if (prop.time) {
                return ( 
                  <View key={"v00_"+key} style={ styles.time }>
                    <View style={ styles.timePart }>
                      <Image source={ require('./public/up.png')}/>
                      <Text style={ styles.timePartValue }>{ this.state.selHours < 10 ? "0" : ""}{ this.state.selHours }</Text>
                      <Image source={ require('./public/down.png')}/>
                    </View>
                    <View>
                      <Text style= { styles.timePartUnit }>H</Text>
                    </View>
                    <View style= { styles.timePart }>
                      <Image source={ require('./public/up.png')}/>
                      <Text style={ styles.timePartValue }>{ this.state.selMinutes < 10 ? "0" : ""}{ this.state.selMinutes }</Text>
                      <Image source={ require('./public/down.png')}/>
                    </View>
                    <View>
                      <Text style={ styles.timePartUnit }>M</Text>
                    </View>
                    <View style={{ borderRadius: hp('9%') }}>
                      <View style= { styles.button }>
                        <Text style={ styles.buttonText }>✓</Text>
                      </View>
                    </View>
                  </View>
                )
              }

              // choice buttons with numbers
              else if (prop.numberChoices) {
                return (
                  <View key={"v00_"+key} 
                        style = {{ flexDirection: 'row', justifyContent: 'center' }}>
                        { prop.numberChoices.map((v,k) => {
                          return (
                            <TouchableOpacity
                                key={"touchable_"+key}
                                onPress = {  () => {  this.Call_DialogFlow_Api(v); }}>
                              <View key={"viewbutton_"+key} 
                                  style={ [styles.choiceText, { marginLeft: wp('1%'), marginRight: wp('1%'), marginBottom: hp('2%')}] }>
                                <Text style= {{ textAlign: 'center' }}>{v}</Text>
                              </View>
                            </TouchableOpacity>
                        )})
                        }
                  </View>
                )
              }

              // picker choices
              else if (prop.pickerChoices) {
                  this.pickerOptions = [{ label : 'Sélectionner une réponse', value : null}];
                  return (
                    <Picker
                      onValueChange={(val, ind) => { if (val !== null) this.Call_DialogFlow_Api(val); }}
                    >
                    <Picker.Item 
                      label="Sélectionner une réponse" 
                      value={null} 
                      />
                      { prop.pickerChoices.map((v, k) => {
                          this.pickerOptions.push({label :v, value : v});
                          return 
                            <Picker.Item label={ v } value= { v } />
                          })
                      }
                    </Picker>
                  )
              }
              else if (prop.choice && prop.image && prop.unset==true) {
                return <View key={"v00_"+key} style={styles.choice}>        
                        <View key={key} style={styles.choiceCadre}>                 
                          <TouchableOpacity onPress={ () => { this.state.imagesRobotMessage = prop.choice; this.state.imagesRobot = prop.image;  this.showScaleAnimationDialog()}}>   
                            <Image  source={{ uri:'http://jules.info-rapide.fr/image?code='+prop.image+''}} style={styles.imageChoice}/>
                          </TouchableOpacity>              
                          <TouchableOpacity onPress={ () => { for (let o of data) o.unset=false;  this.Call_DialogFlow_Api(prop.choice); } }> 
                            <Text style={styles.choiceTextWithImage}>{  prop.choice }</Text>
                          </TouchableOpacity>           
                        </View>
                      </View>
              }
              // choice buttons with pictures after answer (disabled)
              else if (prop.choice && prop.image && prop.unset==false) { 
                return  <View key={"v00_"+key} style={styles.choice}>
                          <View key={key} style={styles.choiceCadre}>   
                            <TouchableOpacity onPress={ () => { this.state.imagesRobotMessage = prop.choice; this.state.imagesRobot = prop.image;  this.showScaleAnimationDialog()}}>   
                              <Image  source={{ uri:'http://jules.info-rapide.fr/image?code='+prop.image+''}} style={styles.imageChoice}/>
                            </TouchableOpacity>
                            <Text style={styles.choiceTextWithImage}>{ prop.choice }</Text>
                          </View>
                        </View>   
              }

              // choice button without pictures before answer (enabled)
              else if  (prop.choice && prop.unset){
                 
                return <View key={"v00_"+key} style={styles.choice}>
                          <TouchableOpacity onPress={ () => {   if (prop.choice.toLowerCase().trim() === 'autre') this.setModalVisible(); else { if (data[key]) data[key].unset = false ; this.Call_DialogFlow_Api(prop.choice); }} }>
                            <View style={styles.choiceText}>
                              <Text style={{ textAlign: 'center' }}>{ prop.choice.replace('§','') }</Text>
                            </View>
                          </TouchableOpacity>
                        </View> 
              }

              // choice buttons without pictures after answer (disabled)
              else if  (prop.choice && prop.unset==false) { 
                return <View key={"v00_"+key} style={styles.choice}> 
                            <View style={styles.choiceText}>
                              <Text>{ prop.choice }</Text>
                            </View>
                        </View> 
              }

              else { 
                return <View key={"v00_"+key}></View> 
              }
            }) 
            }

          </View>  

      </ScrollView>

      {!this.state.questionChoices &&
         <View style={ styles.bottomView}>
    {/* text input (page bottom) */}
    <TextInput style={ styles.inputbottom}
    ref={ (ref) => {this.inputAns = ref; } }
    value = { () => { this.state.msgInput }}
    onChangeText = { (v) => { this.state.msgInput =v }}
    underlineColorAndroid={'transparent'} 
    multiline = {true}
    keyboardType = { this.state.keyboardType }
    placeholder= {'Entrer votre message ici'}
    placeholderTextColor= {'#a7a7a7'}
    clearButtonMode = { 'always' }
    autoCorrect={false}
    onSubmitEditing={() => {
      if (this.inputAns != null) {
        if (Platform.OS === 'ios') this.inputAns.setNativeProps({ text: ' ' });
        else this.inputAns.clear();
        setTimeout(() => { this.inputAns.setNativeProps({ text: '' }) }, 5);

      }
      //this.setState( { keyboardType : "default" , isLoading : true } );
    }}
  />

  {/* submit button */}
  <TouchableOpacity style={{ borderRadius: hp('9%')}}
    onPress={ () => {
      if (this.state.msgInput) {
        this.Call_DialogFlow_Api(this.state.msgInput); 
        //Keyboard.dismiss();
        //this.inputAns.clear(); 
        if (this.inputAns != null) {
        if (Platform.OS === 'ios') this.inputAns.setNativeProps({ text: ' ' });
        else this.inputAns.clear();
        setTimeout(() => { this.inputAns.setNativeProps({ text: '' }) }, 5);
        }
        this.setState({msgInput : "", isLoading : true}); 
        //this.setState( { keyboardType : "default" , isLoading : true } );
      }
    }
  }
    title={ 'OK' }>
    {/* <View style= {{ width: hp('11%'), height: hp('11%'), borderRadius: hp('11%') }} shouldRasterizeIOS={false}><Image source={ require('./public/right2.png')} style={{ flex: 1, resizeMode: 'contain' }}/></View> */}
    <View style= { styles.buttonBottom }><Text style={ styles.buttonText }>Envoyer</Text></View>
  </TouchableOpacity>
</View>
      }

    </KeyboardAvoidingView>

  );
  }
}