import * as React from 'react';
import { Text, View, Image, TouchableOpacity, TextInput, Alert, KeyboardAvoidingView, ScrollView} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import NavigationBar from 'react-native-navbar';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { createStackNavigator } from 'react-navigation';
import styles from "./style/UserPageModify_style"

class UserPageModifiy extends React.Component {
  constructor(props) {
    super(props)
    this.state = { 
      Email: global.Email,
      Nom: "",
      Prenom: "",
      Password: "",
      Metier: "",
      Allow: false,
      isLoading: true,
      isLoading2: true,
      isDone: false,
      Apireturn: null,
      Apireturn2: null 
    }
  }
  static navigationOptions = {
    header: null,
  }
  //Api route that return the user infos
  get_UserInfos = async (user)=>{
    console.log("id = ",global.id)
    fetch(
      "http://jules.info-rapide.fr/Infos",
      {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify({Email: global.Email})
      }
    )
    .then((response) =>   response.json() )
    .then((response2) => {
      this.setState({
        isLoading: false, 
        Apireturn: JSON.stringify(response2) 
      })
    })
    .catch(function(error) {
      console.log(error);
    })
  }

  Modify_UserInfos = async ()=>{
    
    fetch(
      "http://jules.info-rapide.fr/Modify",
      {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            id: global.id,
            Email: this.state.Email,
            Prenom: this.state.Prenom,
            Nom: this.state.Nom,
            Password: this.state.Password,
            Metier: this.state.Metier
          })
      }
    )
    // .then(Alert.alert("Modification réussite", "Vos informations ont bien été modifiées"))
    .then((response) =>   response.json() ) 
    .then((response2) => {
      this.setState({
        isLoading2: false, 
        Apireturn2: JSON.stringify(response2) 
      }) 
    })
    .catch(function(error) {
      console.log(error);
    })
  }

  componentWillMount() {
    var user = {
      id: global.id
      ,
    }
    user = JSON.stringify(user)
    this.get_UserInfos(user);
  }

  componentDidUpdate() {
      console.log("update")
      console.log(this.state.Nom)
  }

  render() {
    const { navigate } = this.props.navigation;
    const mytitle = <Text style={styles.titleNav}>Retour </Text>
    const deco = <Text style={styles.titleNav}>Deconnexion </Text>
    //---<Navbar config>---
    const leftButtonConfig = {
      title: mytitle,
      handler: () => navigate("UserPage"),
    };
    const rightButtonConfig = {
      title: deco,
      handler: () => {navigate("Start2"); global.Email = "";}
    };
    //---</Navbar config>---
    if (!this.state.isLoading) {
      if (this.state.Apireturn !== null) {

        parsedResponse = JSON.parse(this.state.Apireturn)
        this.state.Nom = parsedResponse["Nom"];
        this.state.Prenom = parsedResponse["Prenom"];
        this.state.Email = parsedResponse["Email"];
        console.log("email", this.state.Email)
        this.state.Metier = parsedResponse["Metier"];

        if (parsedResponse.response === 1) {
            Alert.alert("Unexpected Error");
            navigate('Login');
        }
      }
      this.state.Apireturn = null;
    }
    if (!this.state.isLoading2) {
      if(JSON.parse(this.state.Apireturn) == 0) {
        Alert.alert("Modifications Réussite")
      }
      if(JSON.parse(this.state.Apireturn2) == 1) {
        Alert.alert("Erreur","Une erreur est survenue")
      }
    }

    return (
      <KeyboardAvoidingView
                style={styles.container}
                behavior="padding" enabled>
        <NavigationBar
          style={styles.Navbar}
          leftButton={leftButtonConfig}
          rightButton={rightButtonConfig}
        />
        <ScrollView>
        <View style={styles.body}>
          <View style={styles.avatar}>
            <View style={{ borderWidth: 3, borderColor: '#4a90e2', borderRadius: wp('32%')}}><Image source={ require('./public/vous.png')} style={{ width: wp('32%'), height: wp('32%'), resizeMode: 'contain' }}/></View>
              <View style ={styles.allowNotif}>
              </View>
            </View>
            <View style={styles.Form}>
              <TextInput style={styles.Input}
                  underlineColorAndroid= 'transparent'
                  onChangeText={Nom => this.setState({ Nom })}
                  value={this.state.Nom}
                  placeholder= {'Entrer votre nom '}
                  placeholderTextColor= {'#a7a7a7'}
                  controlled={true}>
              </TextInput>       
              

              <TextInput style={styles.Input}
              underlineColorAndroid= 'transparent'
              onChangeText={(prenom) => this.setState({Prenom: prenom})}
                  value={this.state.Prenom}
                  placeholder= {'Entrer votre prénom'}
                  placeholderTextColor= {'#a7a7a7'}
              />

              <TextInput style={styles.Input}
                  underlineColorAndroid= 'transparent'
                  onChangeText={Email => this.setState({ Email })} 
                  value={this.state.Email}
                  placeholder= {'Entrer votre adresse mail'}
                  placeholderTextColor= {'#a7a7a7'}
                  autoCapitalize = "none"
              />

              <TextInput style={styles.Input}
                  underlineColorAndroid= 'transparent'
                  onChangeText={Metier => this.setState({ Metier })} 
                  value={this.state.Metier}
                  placeholder= {'Entrer votre métier'}
                  placeholderTextColor= {'#a7a7a7'}
                  autoCapitalize = "none"
              />

              <TextInput style={styles.Input}
                  underlineColorAndroid= 'transparent'
                  secureTextEntry={true}
                  onChangeText={Password => this.setState({ Password })} 
                  value={this.state.Password}
                  placeholder= {'Entrer votre mot de passe'}
                  placeholderTextColor= {'#a7a7a7'}
                  autoCapitalize = "none"
              />

              <TouchableOpacity onPress = {() => { this.Modify_UserInfos() } }>
                <View style={styles.Button}>
                  <Text style={{ color: '#ffffff', fontSize: hp('2.7%')}}>Mettre à jour</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.footer}>
            </View>
            <View style={ styles.copyrightView }>
              <Text style={ styles.copyright }> ®2018 - LesJules </Text>
            </View>
          </View>
          </ScrollView>
          </KeyboardAvoidingView>
      );
    }
}


export default UserPageModifiy;