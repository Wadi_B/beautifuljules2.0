import * as React from 'react';
import { Text, TextInput, Image, View, ScrollView, StyleSheet,Platform, TouchableOpacity, Alert, KeyboardAvoidingView  } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import styles from "./style/SignupForm_style"

class SignupForm extends React.Component {

    static navigationOptions = {
        header: null,
    }

    constructor(props) {
        super(props)
        this.state = { 
            Nom: '',
            Prenom:'', 
            Email: '', 
            Password: '', 
            ConfirmPassword: '',
            text: '',
            isLoading: true,
            isDone: false,
            Apireturn: null,
        }
    }
    inputs = {};
    // function to focus the field
    focusTheField = (id) => {
        this.inputs[id].focus();
    }

    //Calling the register api route
    RegisterApi = async (user)=>{
        fetch(
            "http://jules.info-rapide.fr/Register",
            {
                method: "POST",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                body: user
            }
        )
        .then((response) =>   response.json() )
        .then((response2) => {
            this.setState({
                isLoading: false,
                Apireturn: JSON.stringify(response2) 
            })
        })
        .catch(function(error) {
        Alert.alert("Erreur de reseau");
        })
    }
    
    //Function called after pressing a button
    handleSubmit = (e) => {
        e.preventDefault();
        //check if every input are filled
        if (this.state.Email !== "" && this.state.Password !== "") {
            var user = {
                Email: this.state.Email,
                Password: this.state.Password
            }
            user = JSON.stringify(user)
            var myvalue = this.RegisterApi(user);   
        }
        else {
            Alert.alert(
                'Inscription incomplète',
                'Veuillez remplir tous les champs',
            )
        }
    }
    render() {
        const { navigate } = this.props.navigation;
        if (!this.state.isLoading) {
            if (this.state.Apireturn !== null) {    
                parsedResponse = JSON.parse(this.state.Apireturn);
                console.log(parsedResponse)
                if (parsedResponse.response === 1) {
                    Alert.alert(
                        'Inscription impossible',
                        'Cet Email existe déjà',
                        [
                            {text: 'Annuler',},
                            {text: 'Se connecter', onPress: () => {navigate("Login"); global.Email = this.state.Email}},
                            
                        ],
                        { cancelable: false }
                    )
                }
                else if (parsedResponse[0].response == 0) {
                    Alert.alert("Inscription reussie");
                    global.Email = this.state.Email
                    global.id = parsedResponse[1].id
                    navigate('DialogFlow');
                }
            }
            this.state.Apireturn = null;
        }
        return (
            <KeyboardAvoidingView  contentContainerStyle={{flex:1}} scrollEnabled={true}
            style= {{flex: 1, backgroundColor: '#ffffff'}}
            behavior="position"
            enableAutoAutomaticScrol='true'
            keyboardOpeningTime={0}>
            <View>
                <ScrollView style={{backgroundColor: "white"}}>
                    <View style={styles.avatar}>
                        <Image source={ require('./public/logo.png')} style={{ width: wp('39 %'), height: wp('39%'), resizeMode: 'contain' }}/>
                        <Text style={styles.Title}>S'inscrire</Text>
                        <Text style={styles.connectDescr}>Inscrivez-vous pour retrouver votre historique de vos travaux chez nos clients.</Text>
                    </View>
                    <View style={styles.Form}>
                        <TextInput style={styles.Input}
                            underlineColorAndroid= 'transparent'
                            onChangeText={Email => this.setState({ Email })} 
                            value={this.state.Email}
                            placeholder= {'Entrer votre adresse mail'}
                            placeholderTextColor= {'#a7a7a7'}
                            autoCapitalize = "none"
                            blurOnSubmit={ false }
                            returnKeyType={ 'next' }
                            onSubmitEditing={() => { this.focusTheField('Password'); }}
                        />
                        <TextInput style={styles.Input}
                            ref={input => { this.inputs['Password'] = input }}
                            underlineColorAndroid= 'transparent'
                            secureTextEntry={true}
                            onChangeText={Password => this.setState({ Password })} 
                            value={this.state.Password}
                            placeholder= {'Entrer votre mot de passe'}
                            placeholderTextColor= {'#a7a7a7'}
                            autoCapitalize = "none"
                            onSubmitEditing={this.handleSubmit}
                        />
                        <TouchableOpacity onPress = {this.handleSubmit}>
                            <View style={styles.Button}>
                                <Text style={{ color: '#ffffff', fontSize: hp('2.7%')}}>valider</Text>
                            </View>
                        </TouchableOpacity>
                    </View> 
                <View style={styles.footer}>
                    <View style={styles.linksView1}>
                        <Text style={styles.linkText} onPress={ () => navigate('Login') } >J'ai déjà un compte</Text>
                    </View>
                </View>
                <View style={ styles.copyrightView }>
                    <Text style={ styles.copyright }> ®2018 - LesJules </Text>
                </View>
            </ScrollView> 
            </View>           
            </KeyboardAvoidingView>
        );
    }
}

export default SignupForm;
