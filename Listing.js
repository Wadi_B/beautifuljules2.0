import React, { Component } from 'react';
import { Platform, TouchableOpacity, Image, StyleSheet, Text, ScrollView, KeyboardAvoidingView, View } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ProgressCircle from 'react-native-progress-circle'
import NavigationBar from 'react-native-navbar';
import styles from "./style/Listing_style"
import PTRView from 'react-native-pull-to-refresh';
import PopupDialog, {
  SlideAnimation,
  ScaleAnimation,
  FadeAnimation,
} from 'react-native-popup-dialog';

const slideAnimation = new SlideAnimation({ slideFrom: 'bottom' });
const scaleAnimation = new ScaleAnimation();
const fadeAnimation = new FadeAnimation({ animationDuration: 150 });

export default class Listing extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedColor: 'blue',
      selectedColore: 'red',
      isLoading: true,
      dataSource: null,
      gestureName: 'none',
      dialogShow: false,
      imagesRobot : "none",
      imageRobotMessage : "none",
      
      Content: {
        id: "",
        DateDebut: "",
        DateFin: "",
        NomClient: "",
        adresse: "",
        zip: "",
        ville: "",
        travaux: "",
        gps: "",
      },
      uniquevalue: 1,
    }
    myColor = [
      '#F5A9A9',
      '#b8e6f8',
      '#F5BCA9',
      '#F5D0A9',
      '#F3E2A9',
      '#F2F5A9',
      '#E1F5A9',
      '#D0F5A9',
      '#BCF5A9',
      '#A9F5A9',
      '#A9F5D0',
      '#A9F5E1',
      '#A9F5F2',
      '#A9E2F3',
      '#A9D0F5',
      '#A9BCF5',
      '#A9A9F5',
      '#BCA9F5',
      '#D0A9F5',
      '#E2A9F3',
      '#F5A9F2',
      '#F5A9E1',
      '#F6CEE3',
      '#E6E6E6',
    ],
    COLOR = "",
    COLOR1 = "",
    myColor1 = [
      '#A4A4A4',
      '#2E64FE',
      '#2EFEC8',
      '#64FE2E',
      '#FE9A2E',
      '#0174DF',
    ],
    this.scrollview = null;
    data = [];
  } 

  static navigationOptions = {
    header: null 
  }
  //Call to Api that get all the task with the id of the user
  get_userintervention = async (user)=> {
    fetch(
      "http://jules.info-rapide.fr/Get_List_Interventions",
      {
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: user
      }
    )
    .then((response) =>   response.json() )
    .then((response2) => {
      this.setState({
      isLoading: false, 
      dataSource: JSON.stringify(response2)
    })
    })
    .catch(function(error) {
      console.log("Network Error")
    })
  }

  componentDidMount() {
    
  }

  _getRandomColor() {
    var item = this.state.myColor[Math.floor(Math.random()*this.state.myColor.length)];
    this.setState({
      selectedColor: item,
    })
  }

  componentWillMount() {
    global.loaded = 2;
    //getting the id user which is a global we declared after LOGIN
    var user = {
      IDuser: global.id
    };
    user = JSON.stringify(user)
    test = this.get_userintervention(user)
  }

  showScaleAnimationDialog = () => {
      this.setState({isLoading:true}); 
    this.scaleAnimationDialog.show();
  }
  showSlideAnimationDialog = () => {
    this.slideAnimationDialog.show();
  }

  showFadeAnimationDialog = () => {
    this.fadeAnimationDialog.show();
  }
  forceRemount = () => {
    console.log("forceremount")
    this.setState({isLoading: true})
    this.componentWillMount()
    global.loaded = 1
  }

  // different style of displaying the task
  INProgress() {  
    const { navigate } = this.props.navigation;
    if (!this.state.isLoading) {         
      
      if (this.state.dataSource !== null && data !== null) {
        if(this.state.dataSource.map) {
          return this.state.dataSource.map(function(data, i) {

            var i = 0;
            var item = this.myColor[Math.floor(Math.random()*this.myColor.length)];
            while (item == this.COLOR && i < 5) {
              this.myColor[Math.floor(Math.random()*this.myColor.length)];
              i++;
            }
            this.COLOR = item

            tohms = function(data) {
              worki = data;
              h = (worki / 3600) | 0 ;
                worki = worki % 3600;
                m = (worki / 60 ) | 0; 
                s = worki % 60 ;
                if (h >= 10 && m < 10) {
                  hms = h + "h0" + m
                }
                else if (h >= 10 && m >= 10) {
                  hms = h + "h" + m
                }
                else if (h < 10 && m >= 10) {
                  hms = "0" + h + "h" + m
                }
                else if (h < 10 && m < 10) {
                  hms = "0" + h + "h0" + m
                }
                return(hms)
              };            
            if (data.state == 0) {
              return (
                <View key={i}>
                  <TouchableOpacity 
                    onPress={() => { navigate(
                      'WorkDetails',
                      { data } 
                      );
                    }}
                  >
                    <View style= {{
                      paddingTop: hp('1%'),
                      paddingBottom: hp('1%'),
                      paddingLeft: wp('1%'),
                      paddingRight: wp('1%'),
                      borderRadius: 15,
                      marginTop: hp('1%'),
                      marginBottom: hp('1%'),
                      backgroundColor: this.COLOR,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      overflow: 'hidden', 
                    }}>
                      <ProgressCircle 
                        percent={100}
                        radius={40}
                        borderWidth={6} 
                        color="#7c6eda"
                        shadowColor="#999"
                        bgColor={this.COLOR}> 
                          <Text>{tohms(data.duree)}</Text>
                        </ProgressCircle>
                        <View style= { styles.workInfo }>
                          <View style={ styles.workDate}>
                            <Text style={styles.workDateText}>{data.DateDebut}</Text>
                          </View>
                            <View style={ styles.workDescr}>
                              <Text style={styles.workDescrText}>{data.travaux}</Text>
                            </View>
                          </View>
                          <View style={ styles.right }><Image style={ styles.rightImage} source= { require('./public/right3.png')}/>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>  
              );
            }
          });
        }
      }
      else {
        return(
          <Text>Pas de travaux en cour</Text>
        );
      }  
    }
  }
  // different style of displaying the task
  Over() {
    const { navigate } = this.props.navigation;
    if (!this.state.isLoading) {
      if (this.state.dataSource !== null && data !== null ) {
        if(this.state.dataSource.map ) {
          return this.state.dataSource.map(function(data, i) {

            var i = 0;
            var item = this.myColor[Math.floor(Math.random()*this.myColor.length)];

            while (item == this.COLOR1 && i < 5) {
              item = this.myColor[Math.floor(Math.random()*this.myColor.length)];
              i++;
            }
            this.COLOR1 = item

            tohms = function(data) {
              worki = data;
              h = (worki / 3600) | 0 ;
              worki = worki % 3600;
              m = (worki / 60 ) | 0; 
              s = worki % 60 ;
              if (h >= 10 && m < 10) {
                hms = h + "h0" + m
              }
              else if (h >= 10 && m >= 10) {
                hms = h + "h" + m
              }
              else if (h < 10 && m >= 10) {
                hms = "0" + h + "h" + m
              }
              else if (h < 10 && m < 10) {
                hms = "0" + h + "h0" + m
              }
              return(hms)
            };          
            if(data.state == 1) {
              aa = tohms(data.duree);
              return(
                <View key={i}>
                  <TouchableOpacity 
                  //sending the id of the task when clicking on it
                    onPress={() => { navigate(
                      'WorkDetails',
                      { data },
                      data,
                      );
                    }}
                  >
                    <View style= {{
                      paddingTop: hp('1%'),
                      paddingBottom: hp('1%'),
                      paddingLeft: wp('1%'),
                      paddingRight: wp('1%'),    
                      borderRadius: 10,
                      marginTop: hp('1%'),
                      marginBottom: hp('1%'),    
                      backgroundColor: "#eaeaea",
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      overflow: 'hidden',
                    }}>
                      <ProgressCircle
                        percent={100}
                        radius={40} 
                        borderWidth={6}
                        color="#ff9696"
                        shadowColor="#999"
                        bgColor={ "#eaeaea"}>  
                        <Text>{aa}</Text>
                      </ProgressCircle>
                      <View style= { styles.workInfo }>
                        <View style={ styles.workDate}><Text style={styles.workDateText}>{data.DateDebut}</Text></View>
                          <View style={ styles.workDescr}><Text style={styles.workDescrText}>{data.travaux}</Text></View>
                            <View style={ styles.workLocation}>
                              <Text style={ styles.workLocationText }>{data.ville}</Text>
                            </View> 
                          </View>
                          <View style={ styles.right }>
                            <Image style={ styles.rightImage} source= { require('./public/right3.png')}/>
                          </View>
                        </View>
                      </TouchableOpacity>
                    </View>  
              );
            }
          });
        }
        else {
          return(
            <Text>Aucun travaux</Text>
          );
        }
      }
    }
  }

  render(){ 
    if(global.loaded = 0) {
      this.forceRemount()
    }
    const { navigate } = this.props.navigation;
    //Navigation configuration
    const myLeftTitle = <Text style={styles.titleNav}>Retour</Text>;
    const myRightTitle = <Image style={styles.ReloadButton} source={ require('./public/reload.png')} />;
    const leftButtonConfig = {
      title: myLeftTitle,
      handler: () => navigate("DialogFlow"),
    };
    const rightButtonConfig = {
      title: myRightTitle,
      handler: () => { this.forceRemount(); navigate("Listing")},
    };
    if (!this.state.isLoading) {  
      if (this.state.dataSource !== null) {
        this.state.dataSource = JSON.parse(this.state.dataSource);
      }
    }
    return(
      <KeyboardAvoidingView  
        contentContainerStyle={{flex:1}} scrollEnabled={true}
        style= {{flex: 1, backgroundColor: '#ffffff'}}
        behavior="position"
        enableAutoAutomaticScrol='true'
        keyboardOpeningTime={0}
      >
      
        <NavigationBar
          style={styles.Navbar}
          leftButton={leftButtonConfig}
          rightButton={rightButtonConfig}
        />
          <ScrollView  
            style={{flex: 1, backgroundColor: "white"}} 
            ref={ref => this.scrollView = ref}>
            <View style={ styles.header}>
              <Text style= {styles.title }>Mes Travaux</Text>
            </View>
            <View style= {styles.body }>
              <View style= {styles.inProgress}>
              <Text style={{ fontSize: hp('2%')}}>EN COURS</Text>  
                {this.INProgress()}
              </View>
              <View style={styles.finished}>
                <Text style={{ fontSize: hp('2%')}}>TRAVAUX FINIS</Text>
                  {this.Over()}
              </View>
            </View>    
        </ScrollView>
        {/* </PTRView> */}
      </KeyboardAvoidingView>
    );
  }
}
