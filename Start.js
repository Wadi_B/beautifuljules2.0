import React, { Component } from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { TouchableOpacity, StyleSheet, Image } from 'react-native';
import styles from "./style/Start_style"

export default class Start extends React.Component {

  static navigationOptions = {
    header: null 
}

  render() {  
    const { navigate } = this.props.navigation;
    //Display splashcreen during 0.7sec and navigate to Start2
    setTimeout(() => { navigate('Start2')}, 700);
    return(
      <TouchableOpacity onPress= {() => { navigate('Start2') }}  style = { styles.MainContainer } >  
          <Image source={ require('./public/logo.png')} style={styles.image}/>
      </TouchableOpacity>
    );
  }
}

