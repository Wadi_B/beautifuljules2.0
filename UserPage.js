import * as React from 'react';
import { Text, View, ScrollView, Image, Switch, TouchableOpacity, Alert } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import NavigationBar from 'react-native-navbar';
import {
  createStackNavigator,
} from 'react-navigation';
import styles from "./style/UserPage_style"


class UserPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = { 
      Email: global.Email ,
      Nom: '', Prenom: '',
      Allow: false,
      gestureName: 'none',
      isLoading: true,
      isDone: false,
      Apireturn: null
    }
  }

  static navigationOptions = {
    header: null,
  }

  //Calling api route that return user info
  get_UserInfos = async (user) => {
    fetch(
      "http://jules.info-rapide.fr/Infos",
      {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        body: user
      }
    )
    .then((response) =>   response.json() )
    .then((response2) => {
      this.setState({
        isLoading: false, 
        Apireturn: JSON.stringify(response2) 
      })
    })
    .catch(function(error) {
      console.log(error);
    })
  }

  componentWillMount() {
    var user = {
      Email: this.state.Email,
    }
    user = JSON.stringify(user)
    var myvalue = this.get_UserInfos(user);     
  }
  render() {
    const { navigate } = this.props.navigation;
    //---<Navbar config>---
    const myLeftTitle = <Text style={styles.titleNav}>Retour</Text>;
    const myRightTitle = <Text style={styles.titleNav}>Editer</Text>;
    const rightButtonConfig = {
      title: myRightTitle,
      handler: () => {navigate("UserModify");}
    };
    const leftButtonConfig = {
      title: myLeftTitle,
      handler: () => {navigate("DialogFlow");}
    };
    //---</Navbar config>---
    if (!this.state.isLoading) {
      if (this.state.Apireturn !== null) {
        parsedResponse = JSON.parse(this.state.Apireturn)
        this.state.Nom = parsedResponse["Nom"];
        this.state.Prenom = parsedResponse["Prenom"];
        this.state.Email = parsedResponse["Email"];
        if (parsedResponse.response === 1) {
            Alert.alert("Unexpected Error");
            navigate('Login');
        }
      }
      this.state.Apireturn = null;
    }
    return (
      <View style={styles.container}>
          <NavigationBar
            style={styles.Navbar}
            rightButton={rightButtonConfig}
            leftButton={leftButtonConfig}
          />
            <View style={{alignItems: "center"}}>
                <View style={styles.image}>
                  <TouchableOpacity onPress={() => {navigate("UserModify")}}>
                    <View style={{  borderRadius: wp('32%')}}><Image
                        source = { require('./public/vous.png') }
                        style= {styles.myimage} />
                        </View>
                  </TouchableOpacity>
                  </View>
                  <View style={styles.Noms}>
                    <Text style={styles.UserName}>{this.state.Prenom} {this.state.Nom}</Text>
                    <Text style={styles.Email}>{this.state.Email}</Text>
                    <Text style={styles.Signout} onPress={ () => {navigate("Start2"); global.Email = "";}}>se déconnecter</Text>
                  </View>
                </View>
                <View style={styles.FormCenter}>
                  <View style={{alignItems: "center"}}>
                    <Text style={styles.FormTitle}>Autoriser les notifications</Text>
                    <Switch
                      onTintColor= {'#0077cc'}
                      style={{top: hp("8%")}}
                      onValueChange={() => { this.Allow = !this.Allow }}
                    />
                    <View style={styles.image1}>
                      <Image source={ require('./public/logo.png')} style={ {width: hp('13%'), height: hp('13%'), resizeMode:'contain', bottom: hp("3%")} } />
                    </View>
                    <Text style={styles.FooterLink}>CGU - CGV</Text>
                </View>
                <View></View>
            </View>
        </View>
      );
    } 
  }


export default UserPage;
