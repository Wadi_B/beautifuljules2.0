import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { TouchableOpacity,  StyleSheet, Text, View, Image } from 'react-native';
import styles from "./style/Start2_style"

export default class Start extends React.Component {
  static navigationOptions = {
    header: null 
  }
  render(){  
    const { navigate } = this.props.navigation;
    return(
      <View style = { styles.MainContainer }>
        <View style = {{alignItems: 'center'}}>
          <View style={styles.image}>
            <TouchableOpacity> 
              <Image source={ require('./public/logo.png')} style={ {width: wp('50%'), height: hp('28%'), resizeMode: 'contain' } }  />
            </TouchableOpacity>
          </View>
          <View style={ styles.Descr1 }>
            <Text style={ styles.descr1 }>Plus de 10 000 clients font confiance aux Jules depuis 2009 (v0.1.9)</Text>
          </View>
          <View style={ styles.Descr2 }>
            <Text style={ styles.descr2 }>Les Jules, des bricoleurs professionnels certifiés.</Text>
          </View>
        </View>
        <View style={ styles.linksView1}>
          <Text style={ styles.linkText } onPress={ () => { navigate('Login'); }}>Se connecter</Text>
        </View>
        <View style={ styles.linksView2}>                                 
          <Text style={ styles.linkText } onPress={() => { navigate('SignupForm');}}>S'inscrire</Text>
        </View>
      </View>
    );
  }
}