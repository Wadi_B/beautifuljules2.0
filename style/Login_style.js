import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default StyleSheet.create({
    Modale: {
        marginTop: 22,
        alignItems: "center",
        width: wp("65%"),
        height: hp("20%"),
        top: hp("10%"), 
        backgroundColor: "white",
        shadowColor: 'black',
        shadowOffset: {
            width: 1,
            height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
        marginBottom: hp("3%")
    },
    titlemodal: {
        fontSize: hp("3%"),
        top: hp("2%")
    },
    modaleinput: {
        top: hp("3%"),
        fontSize: hp("3%"),
        justifyContent: "center"
    },
    touchablebuttonmodal: {
        top: hp("6%"),
        alignItems: "center",
        justifyContent: "center",
        width: wp("20%"),
        height: hp("4%"),
        backgroundColor: "#4a90e2",
        borderRadius:5,
    },
    Buttonmodal: {
        alignItems: "center",
        width: wp("20%"),
        height: hp("4%"),
    },
    container: {
        flex: 1,
    },
    image: {
        top: hp("10%"),
        marginBottom: hp("5%")
    },
    hairline: {
    backgroundColor: '#dadada',
    left: wp("40%"),
    height: 1,
    width: wp("13%"),
    },
    Title: {
        textAlign: 'center',
        fontSize: hp("4%"),
        fontFamily: "CircularBold",
        top: hp("6%"),
        marginBottom: hp("6%"),
        color:"#413d3d",
    },
    ConnectView: {
        alignItems: "center",
        height: hp("5%"),
        width: wp("70%"),
    },
    connect: {
        fontSize: hp('4.8%'),
        fontFamily: "CircularBold",
        color: "#413d3d",
        textAlign: "center",
    },
    ConnectDescrView: {
        alignItems: "center",
        top: hp("2.5%"),
        width: wp('77.33%'),
    },
    connectDescr: {
        textAlign: 'center',
        fontSize: wp('4.07%'),
        color: "#413d3d",
    },
    Form: {
        top: hp("6%"),
        justifyContent: 'center',
        alignItems: "center",
    },
    input: {
        height: hp("7%"),
        width: wp("85%"),
        paddingLeft: 10,
        fontSize: wp('4.5%'),
        alignItems: "stretch",
        borderRightWidth: 1,
        borderLeftWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 50,
        borderColor: "#c0bdbd",
        fontFamily: "Circular",
        top: hp("1.3%"),
        borderRadius: 5,
        marginBottom: hp("2%"),
    },
    Button: {
        marginTop: hp("2%"),
        alignItems: "center",
        left:wp("5%"),
        justifyContent: "center",
        width: wp("70%"),
        height: hp("7%"),
        backgroundColor: "#4a90e2",
        borderRadius:5,
        marginBottom: hp("8%")
    },
    touchablebutton: {
        width: wp("75%"),
        height: hp("10%"),
        marginBottom: hp("5%")
    },
    linksView1: {
        top: hp("5%"),
        height: hp("3%"),
        width: wp("44%"),
        alignItems: 'center',
        justifyContent: 'center',
        left: wp("2%"),
    },
    linksView2: {
        top: hp("2%"),
        height: hp("3%"),
        width: wp("44%"),
        left: wp("44%"),
        right: wp("3%"),
        alignItems: 'center',
        justifyContent: 'center',
    },
    //manque l'ajout de police
    linkText:{
        textDecorationLine: 'underline',
        color: '#4a90e2',
        fontSize: wp('4%'),
    },
    copyrightView: {
        alignItems: "center",
        marginTop: hp("13%"),
    },
    copyright: {
        color: "#aeaaaa",
        alignItems: "center",
        bottom: hp("0%"),
    }
    });