import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default StyleSheet.create({
    MainContainer:
    {
        flex: 1,
        backgroundColor: "#ffffff",
    },
    image: 
    {
        width: wp('50%'),
        height: hp('28%'),
        top: hp("12%"),
    },
    descr: {
        left: wp("11.3%")
    },
    Descr1: {
        height: hp("13.1%"),
        width: wp("77.3%"),
        top: hp("25%"),
    },
    descr1: {
        fontSize: wp('4%'),
        fontFamily: "CircularBold",
        color: '#444444',
        textAlign: 'center',
        justifyContent: "center",
    },
    Descr2: {
        height: hp("6%"),
        width: wp("65%"),
        top: hp("24%"),
    },
    descr2: {
        fontSize: wp('4%'),
        color: '#444444',
        textAlign: 'center',
        justifyContent: "center",
    },
    linksView1: {
        top: hp("40%"),
        height: hp("3%"),
        width: wp("44%"),
        alignItems: 'center',
        justifyContent: 'center',
        left: wp("3%")
    },
    linksView2: {
        top: hp("37%"),
        height: hp("3%"),
        width: wp("44%"),
        left: wp("58%"),
        right: wp("3%"),
        alignItems: 'center',
        justifyContent: 'center',
    },
    //manque l'ajout de police
    //font size en EM
    linkText:{
        textDecorationLine: 'underline',
        color: '#4a90e2',
        fontSize: wp('4.4%'),
    },
});
