import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5',
    },
    image: {
        top: hp("1%"),
    },
    title: {
        fontSize: hp('4.5%'),
        fontFamily: "CircularBold",

        color: '#413d3d',
    },
    titleDescrView: {
        height: hp("11%"),
        width: wp("73%"),
        top: hp("2.7%"),
    },
    titleDescr: {
        textAlign: 'center',
        fontSize: wp('4.07%'),
        color: '#413d3d',
    },
    Form: {
        top: hp("10%"),
        width: wp("100%"),
        height: hp("15%"),
        backgroundColor: "#ffffff",
    },
    input: {
        height: hp("12%"),
        width: wp("80%"),
        left: wp("9%"),
        top: hp("5%"),
    },
    touchableButton: {
        alignItems: "center",
        top: hp("20%")
    },
    Button: {
        alignItems: "center",
        justifyContent: "center",
        width: wp("64%"),
        height: hp("8%"),
        backgroundColor: "#4a90e2",
        borderRadius:45,
    },
    linksView1: {
        top: hp("29%"),
        height: hp("3%"),
        width: wp("44%"),
        alignItems: 'center',
        justifyContent: 'center',
        left: wp("9.2%"),
    },
    linksView2: {
        top: hp("26%"),
        height: hp("3%"),
        width: wp("44%"),
        left: wp("49%"),
        alignItems: 'center',
        justifyContent: 'center',
    },
    linkText:{
        textDecorationLine: 'underline',
        color: '#4a90e2',
        fontSize: wp('4%'),
    },
});