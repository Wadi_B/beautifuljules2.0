import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  Navbar: {
    marginTop: hp('3%'),
    backgroundColor: '#ffffff',
    height: hp("6%"),
  },
  titleNav: {
    color: "black"
  },

  image: {
    height: hp('16.75%'),
    width: hp('16.75%'),
    top: hp("5%"),
    borderRadius: wp('32%'),
    resizeMode: 'contain',
    borderWidth: 3,
    borderColor: '#4a90e2',
  },
  myimage: {
    height: hp('16%'),
    width: hp('16%'), 
  },
  Noms: {
    top: hp("10%"),
    alignItems: "center",
    justifyContent: "center",
  },
  UserName: {
    fontSize: hp('3%'),
    fontWeight: 'bold',
    textAlign: 'center',
    color: "#413d3d",
  },
  Email: {
    color: "#413d3d",
    top: "1.3%",
    fontSize: hp("2.3%"),
  },
  Signout: {
    top: hp('1.60%'),
    color: "#ff9696",
    fontSize: hp("2.3%"),
    textDecorationLine: 'underline',
  },
  image1: {
    height: hp('16%'),
    width: hp('16%'),
    top: hp("10%"),
    borderRadius: wp('32%'),
    resizeMode: 'contain',
  },
  FormCenter: {
    top: hp("22%"),
    backgroundColor: "#f5f5f5",
    width: wp("100%"),
    height: hp("100%"),
  },
  FormTitle: {
    fontSize: hp("2.3%"),
    top: hp("6.2%"),
  },
  FooterLink: 
  {
    top: hp('13.5%'),
    fontSize: hp("2.2%"),
    color: "#413d3d",
    textDecorationLine: "underline",
    opacity: 0.5,
  }
});
