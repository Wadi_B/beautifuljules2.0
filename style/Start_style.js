import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default StyleSheet.create({
    MainContainer:
    {
        flex: 1,
        alignItems: 'center',
        backgroundColor: "white"
    },
    image: {
        top: hp('27.1%'),
        backgroundColor: "white",
    }
    });