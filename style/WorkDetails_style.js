import {StyleSheet, Platform} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default StyleSheet.create({
    Navbar: {
        marginTop: hp('3%'),
        backgroundColor: '#ffffff',
        height: hp("6%"),
    },
    // --- MODALE --- //
    Modale: {
        marginTop: 11,
        alignItems: "center",
        textAlign: "center",
        width: wp("70%"),
        ...Platform.select({
            ios: {
                height: hp("30%"),
            },
            android: {
                height: hp("35%"),
            },
            }),
        top: hp("15%"), 
        backgroundColor: "white",
        shadowColor: 'black',
        shadowOffset: {
            width: 1,
            height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 1.0,
        marginBottom: hp("3%"),
        borderRadius: 5
    },
    titlemodal: {
        justifyContent: "center",
        fontSize: hp("3%"),
        top: hp("2%")
    },
    modaleinput: {
        top: hp("3%"),
        fontSize: hp("3%"),
        justifyContent: "center"
    },
    touchablebuttonmodal: {
        top: hp("6%"),
        alignItems: "center",
        justifyContent: "center",
        width: wp("25%"),
        height: hp("4%"),
        backgroundColor: "#4a90e2",
        borderRadius:5,
    },
    Buttonmodal: {
        alignItems: "center",
        width: wp("25%"),
        height: hp("4%"),
    },
    buttonvalider: {
        borderRadius: 5,
        overflow: 'hidden',
        // flexDirection: 'row',
        // position: "absolute",
        //top: wp("4%"),
        // right: wp("10%"),
    },
    buttonTextvalider: {
        paddingTop: hp('1%'),
        paddingBottom: hp('1%'),
        paddingLeft: wp('2%'),
        paddingRight: wp('2%'),
        backgroundColor: '#4a90e2',
        color: '#ffffff',
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: wp('4%'),
    },
    buttonclose: {
        borderRadius: 5,
        overflow: 'hidden',
        flexDirection: 'row',
        marginRight: wp('3%'),
    },
    buttonTextclose: {
        paddingTop: hp('1%'),
        paddingBottom: hp('1%'),
        paddingLeft: wp('2%'),
        paddingRight: wp('2%'),
        backgroundColor: '#4a90e2',
        color: '#ffffff',
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: wp('4%'),

    },
    // --- MODALE --- //

    // --- TIMER --- //
    time: {
        marginTop: hp('5%'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: "red",
        
    },
    timePart: {
        alignItems: 'center',
        marginLeft: wp('1%'),
        marginRight: wp('1%'),
    },
    timePartValue: {
        fontSize: hp('7.5%'),
        color: '#f5a623',
    },
    timePartUnit: {
        color: '#f5a623',
    },

    // --- TIMER --- //
    titleNav: {
        color: "#515151"
    },
        container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',  
    }, 

    header: {
        alignItems: 'center',
        backgroundColor: '#b8e6f8',
        paddingTop: hp('5%'),
        paddingBottom: hp('5%'),
        },
    title: {
        fontSize: wp('7%'),
        fontWeight: 'bold',
        color: '#413d3d',
        width: wp("80%"),
        borderRadius: 5,
        textAlign: "center",
        justifyContent: "center",
    },    
    body: {
        marginBottom:hp('2%'),
        paddingLeft: wp('5%'),
        paddingRight: wp('5%'),
        backgroundColor: "white",
    },
    mytitle: {
        marginTop: hp("3%"),
        justifyContent: "center",
        alignItems: "center",
        height: hp("5%"),
        width: wp("100%"),
        fontSize: hp("8%"),
        left: wp('-5%')
    },
    mytitle1: {
        marginTop: hp("2%"),
        justifyContent: "center",
        alignItems: "center",
        height: hp("5%"),
        width: wp("100%"),
        fontSize: hp("8%"),
        //left: wp('-5%')
    },
    partTitle: {
        marginTop: hp('3%'),
    },
    adressView: {
        marginTop: hp("3%"),
        borderRadius: 10,
        backgroundColor: '#f5f5f5',
        padding: hp('2%'),
    },
    save: {
        alignItems: "center",
        paddingTop: hp("0.7%"),
        marginTop: hp("2%"),
        width: wp("30%"),
        height: hp("4%"),
        borderRadius: 5,
        backgroundColor: "#4a90e2",

    },
    touchablebutton: {
        height: hp("14%"),
        width: wp("30%"),
        backgroundColor: "red",
    },
    discussion: {
        marginTop: hp('4%'),
        marginLeft: wp('4%'),
        marginRight: wp('4%'),
    },
    note: {
        textAlignVertical: 'top',
    },
    clientName: {
        fontWeight: 'bold',
        color: '#413d3d',
    },
    Button: {
        alignItems: "center",
        left:wp("10%"),
        justifyContent: "center",
        width: wp("70%"),
        height: hp("7%"),
        backgroundColor: "#4a90e2",
        borderRadius:5,
        marginBottom: hp("8%")
    },
    timerView: {
        marginTop: hp("1%"),
        marginBottom: hp("5%"),
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    timerText: {
        color: '#f5a623',
        fontSize: hp('7%'),
    },
    timerUnit: {
        color: '#f5a623',
    },
    buttons: {
        justifyContent: 'center',
        flexDirection: 'row',
    },
    dialogQ: {
        flexDirection: 'row'
        //backgroundColor: '#cccccc'
    },
    dialogA: {
        flexDirection:  'row'
        //backgroundColor: '#aaaaaa'
    }
});
