import {Platform, StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default StyleSheet.create({
  Navbar: {
    marginTop: hp('3%'),
    backgroundColor: '#ffffff',
    height: hp("6%"),
  },
  titleNav: {
    color: "#515151"
  },
  Modale: {
    width: wp("70%"),
    ...Platform.select({
      ios: {
        top: hp("-5%"),
      },
      android: {
        top: hp("2%"),
      },
    }), 
    height: hp('20%'),
    backgroundColor: "white",
    shadowColor: 'black',
    shadowOffset: {
        width: 1,
        height: 3
    },
    alignItems: 'center',
    shadowRadius: 5,
    shadowOpacity: 1.0,
    marginBottom: hp("3%"),
    borderRadius: 5
},
titlemodal: {
  fontSize: hp("3%"),
  top: hp("2.5%"),
  marginBottom: hp("1%")
},
modaleinput: {
  textAlign: "center",
  width: wp("40%"),
  top: hp("3%"),
  fontSize: hp("2%"),
  paddingTop: hp("1%"),
  paddingBottom: hp("1%"),
  ...Platform.select({
    ios: {
      marginBottom: hp("2%"),
    },
    android: {
      top: hp("2%"),
    },
  }),
},
touchablebuttonmodal: {
  top: hp("4%"),
  alignItems: "center",
  justifyContent: "center",
  width: wp("20%"),
  height: hp("4%"),
  backgroundColor: "#4a90e2",
  borderRadius:5,
  marginLeft: wp('1%'),
  marginRight: wp('1%')
},
Buttonmodal: {
  alignItems: "center",
  justifyContent: "center",
  width: wp("20%"),
  height: hp("4%"),
},

    container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',  
  },
  header: {
	},
	backArrow: {
		justifyContent: 'center',
		alignItems: 'center',
    width: 30
  },
	left: {
		flex: 0.5,
		backgroundColor: 'transparent',
	},
  dialogContentView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 15 
  },
  navigationTitle: {
    padding: 10,
  },
  navigationButton: {
    padding: 10,
  },
  navigationLeftButton: {
    paddingLeft: 20,
    paddingRight: 40,
  },
  navigator: {
    flex: 1,
    // backgroundColor: '#000000',
  },
  MainContainer:
  {
    flex: 1,
  },
  body: {
    marginBottom:hp('8%'),
    flex: 1,
  },
  header: { 
    alignItems: 'center',
    marginBottom: hp('2%'),
  },
  title: {
    fontSize: wp('7%'),
    fontWeight: 'bold',
  },
  descr: {
    fontSize: wp('4%'),
  },
  vous: {
    flex: 1,
    marginRight: wp('4%'),
    marginTop: hp('1%'),
    marginLeft: wp('25%'),
    marginBottom: hp('1%'),
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    flexDirection: 'row',
  },
  vousText: {
    backgroundColor: '#aaddff',
    paddingLeft: wp('4.53%'),
    paddingRight: wp('4.53%'),
    paddingTop: hp('1.95%'),
    paddingBottom: hp('1.95%'),
    borderBottomLeftRadius: 15,
    borderTopLeftRadius : 15,
    borderTopRightRadius : 15,
    overflow: 'hidden',
  },
  julie: {
    alignSelf: 'flex-start',
    alignItems: 'flex-end',
    marginLeft: wp('4%'),
    marginTop: hp('1%'),
    marginRight: wp('25%'),
    marginBottom: hp('1%'),
    flexDirection: 'row'
  },
  julieText: {
    backgroundColor: '#f5f5f5',
    paddingTop: hp('1.5%'),
    paddingBottom: hp('2%'),
    paddingLeft: wp('5.07%'),
    paddingRight: wp('4%'),
    borderBottomRightRadius: 15,
    borderTopLeftRadius : 15,
    borderTopRightRadius : 15,
    // fontSize: hp('2.35%'),
    overflow: 'hidden',
  },
  time: {
    marginTop: hp('3%'),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  timePart: {
    alignItems: 'center',
    marginLeft: wp('1%'),
    marginRight: wp('1%'),
  },
  timePartValue: {
    fontSize: hp('7.5%'),
    color: '#f5a623',
  },
  timePartUnit: {
    color: '#f5a623',
  },
  choice: {
    marginLeft: wp('10%'),
    marginRight: wp('10%'),
    marginTop: hp('1%'),
    marginBottom: hp('1%'),
    
  },
  choiceCadre: {
    backgroundColor: '#aaddff',
     padding: wp('2.5%'),
     borderRadius: 5, 
     flexDirection: 'row',
  },
  choiceText: {
    backgroundColor: '#aaddff',
    padding: wp('2.5%'),
    borderRadius: 5,
    //textAlign: 'center',
    alignItems: 'center',
  },
  choiceTextWithImage:{
    padding: wp('2.5%'), 
    borderRadius: 5,
    textAlign: 'center',
  },
  imageChoice:{
       width: hp('5%'), 
       height: hp('5%'),
       padding: wp('2.5%'),
  },
  imageChoiceFull :
  {
      width: hp('25%'), 
       height: hp('25%'),
       paddingTop: wp('2.5%') 
  },
  bottomView:{
    flex: 0.1,
    width: wp('100%'), 
    height: hp('7.49%'), 
    backgroundColor: '#f5f5f5',
    paddingBottom : Platform.OS === 'ios' ? hp('1%') : hp('0%'),
    flexDirection: 'row',
    left: wp('0%'),
    right: wp('0%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputbottom : {
    paddingTop: Platform.OS === 'ios' ? hp('2%') : hp('1.4%'),
    paddingBottom: Platform.OS === 'ios' ? hp('2%') : hp('1.4%'),
    paddingLeft: wp('2%'),
    paddingRight: wp('2%'),
    width : wp('72.53%'),
    marginRight: wp('2.13%'),
    fontSize: wp('4%'),
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: "#c0bdbd",
    borderRadius: 5,
    //fontFamily: "CircularBold",
    //marginBottom: hp("0.5%"), 
  },
  button: {
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
    marginLeft: wp('2%'),
  },
  buttonBottom : {
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'hidden',
  },
  buttonText: {
    paddingTop: hp('2%'),
    paddingBottom: hp('2%'),
    paddingLeft: wp('4%'),
    paddingRight: wp('4%'),
    backgroundColor: '#4a90e2',
    color: '#ffffff',
    textAlign: 'center',
    justifyContent: 'center',
    fontSize: wp('4%'),
  }
  
});


