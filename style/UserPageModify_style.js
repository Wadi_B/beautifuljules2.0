import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default StyleSheet.create({
    Navbar: {
        marginTop: hp('3%'),
        backgroundColor: '#ffffff',
        height: hp("6%"),
    },
    body:{
        backgroundColor: "white",
    },
    titleNav: {
        color: "#515151"
    },
    container: {
        flex: 1,
    },
    avatar: {
        alignItems: 'center',
        marginTop: hp("5%"),
    },

    connectDescr: {
        width: wp('77.33%'),
        textAlign: 'center',
        fontSize: wp('4.4%'),
        marginTop: hp('1.4%'),
        color: '#413d3d',
    },
    Button: {
        justifyContent: "center",
        alignItems: "center",
        width: wp('70%'),
        height: hp('7%'),
        borderRadius: 5,
        alignItems: 'center',
        backgroundColor: '#4a90e2',
        marginTop: hp("4%")
    },
    Form: {
        paddingTop: hp('2.7%'),
        alignItems: "center",
        marginBottom: hp("3%")
    }, 
    Input: {
        height: hp("7%"),
        width: wp("85%"),
        paddingLeft: 10,
        fontSize: wp('4.5%'),
        alignItems: "stretch",
        borderRightWidth: 1,
        borderLeftWidth: 1,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        height: 50,
        borderColor: "#c0bdbd",
        fontFamily: "Circular",
        top: hp("1.3%"),
        borderRadius: 5,
        marginBottom: hp("2%"),
    },
    footer: {
        alignItems: 'center',
    },
    linksView1: {
        top: hp("1%"),
        height: hp("3%"),
        width: wp("44%"),
        alignItems: 'center',
        justifyContent: 'center',
    },
    linksView2: {
        height: hp("3%"),
        width: wp("44%"),
        left: wp("44%"),
        right: wp("3%"),
        alignItems: 'center',
        justifyContent: 'center',
    },  
    linkText:{
        textDecorationLine: 'underline',
        color: '#4a90e2',
        fontSize: wp('4%'),
    },
    copyrightView: {
        alignItems: "center",
        marginTop: hp("4%"),
    },
    copyright: {
        color: "#aeaaaa",
        alignItems: "center",
}
});