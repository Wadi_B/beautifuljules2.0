
import {Platform, StyleSheet} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default StyleSheet.create({
    Navbar: {
        marginTop: hp('3%'),
        backgroundColor: '#ffffff',
        height: hp("6%"),
    },
    titleNav: {
        color: "#515151"
    },
        container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white" 
    },
    ReloadButton: {
    ...Platform.select({
        ios: {
            height: wp("4%"),
            width: wp("4%"),
        },
        android: {
            paddingLeft: wp("50%"),
            height: wp("8%"),
            width: wp("8%")
        },
        }),
    },
    header: {
        alignItems: 'center',
        marginTop: hp('0%'),
        backgroundColor: "white",
        },

    body: {
        marginBottom:hp('8%'),
        flex: 1,
        backgroundColor: "white"
    },
    title: {
        fontSize: wp('7%'),
        fontWeight: 'bold',
        color: '#413d3d',
    },
    inProgress: {
        paddingLeft: wp('5%'),
        paddingRight: wp('5%'),
        marginTop: hp('4%'),
        marginBottom: hp('2.5%'),
    },
    // inProgressOne: {
    //     paddingTop: hp('1%'),
    //     paddingBottom: hp('1%'),
    //     paddingLeft: wp('1%'),
    //     paddingRight: wp('1%'),
    //     borderRadius: 15,
    //     marginTop: hp('1%'),
    //     marginBottom: hp('1%'),
    //     backgroundColor: '#b8e6f8',
    //     flexDirection: 'row',
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     overflow: 'hidden',
    // },
    workInfo: {
        marginLeft: wp('5%'),
        justifyContent: 'center',
        flex: 1,
    },
    workDate: {
        marginBottom: hp('0.5%')
    },
    workDateText: {
        fontSize: hp('1.5%'),
        color: 'rgba('+0x0+','+0x0+','+0+','+'0.4)',
    },
    workDescr: {
        marginTop: hp('0.5%'),
        marginBottom: hp('0.5%'),
    },
    workDescrText: {
        color: '#413d3d',
        fontWeight: 'bold',
        fontSize: hp('2.1%'),
    },
    workLocation: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: hp('0.5%'),
    },
    workLocationText: {
        fontSize: hp('1.5%'),
        color: 'rgba('+0x0+','+0x0+','+0+','+'0.4)',
        fontWeight: 'bold',
        marginLeft: wp('1%'),
    },
    inPauseOne: {
        paddingTop: hp('1%'),
        paddingBottom: hp('1%'),
        paddingLeft: wp('1%'),
        paddingRight: wp('1%'),
        borderRadius: 15,
        marginTop: hp('1%'),
        marginBottom: hp('1%'),    
        backgroundColor: '#bcf8a6',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden',
    },
    inPauseOneText: {
    flex: 1,
        textAlign: 'center',
    },
    pauseIcon: {
        borderRadius: wp('15%'),
        width: wp('15'),
        height: wp('15%'),
        borderWidth: 2,
        borderColor: '#f5a623',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: wp('2%'),
    },
    pauseText: {
        color: '#f5a623'
    },
    finished: {
        paddingLeft: wp('5%'),
        paddingRight: wp('5%'),
        marginTop: hp('2.5%'),
    },
    // finishedOne: {
    //     paddingTop: hp('1%'),
    //     paddingBottom: hp('1%'),
    //     paddingLeft: wp('1%'),
    //     paddingRight: wp('1%'),    
    //     borderRadius: 10,
    //     marginTop: hp('1%'),
    //     marginBottom: hp('1%'),    
    //     backgroundColor: '#eaeaea',
    //     flexDirection: 'row',
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     overflow: 'hidden',
    // },
    finishedOneText: {
        flex: 1,
        textAlign: 'center',
    },
    bottomView:{
    },
    right: {
        justifyContent: 'center',
        right: wp('1%'),
        marginLeft: wp('2%'),
    },
    rightImage: {

    }
});